Compile and run:
To compile and run the client you'll need Microsoft Visual Studio 2013.
Open Visual Studio, go to File -> Open -> Project/Solution (or press ctrl+shift+o) and
go to a directory where you cloned our repository. Open Client/Exception/Exception.sln.

To build a project go to Build -> Build Solution or press F7.

Once solution is built you can run the executable by going to
Debug -> Start Debugging (F5) or
Debug -> Start Without Debugging (ctrl+F5)
depending on what launch option you're interested in.


Game instructions:
To move character around the world use WASD keys.
To fire a laser gun towards cursor's position press left mouse button. Laser gun needs about 1 second to recharge.
After your character is died, press right mouse button to respawn the character.
To write a message in chat press Return key, enter the message and confirm with Return key.
