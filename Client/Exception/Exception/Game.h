#ifndef _Game_h
#define _Game_h

#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <iostream>
#include <set>
#include <memory>

#include "rapidjson/document.h"
#include "rapidjson/prettywriter.h"
#include "rapidjson/stringbuffer.h"

#include "Map.h"
#include "Player.h"
#include "Chat.h"
#include "Bullet.h"
#include "Network.h"
#include "Config.h"
#include "Enemy.h"
#include "EventQueue.h"
#include "HUD.h"

class Game {
public:
	Game();
	Game(std::string serverIP, std::wstring playerName);
	void run();

	void DEBUG_CHAT_EVENT(unsigned int);
	void DEBUG_REMOVE_HIT_POINT(unsigned int);
	void DEBUG_DEATH();

private:
	void processEvents();
	void sendEvents();
	void getServerEvents();
	void processServerEvents();
	void processServerEvent(Event);
	void update();
	void render();
	void handlePressedKey(sf::Event);
	void handleReleasedKey(sf::Event);
	void handleMouseMove(sf::Event);
	void handleMouseButtonPressed(sf::Event);
	void enemyJoined(std::string id, std::string name, sf::Vector2f initPos, bool dead);
	void loadMapFromFile();
	void loadMapFromServer();
	int getPacket(char *, char*);
	void parseChatMessage(rapidjson::Value&);
	void sendChatMessage(std::wstring);
	void sendShotInfo(sf::Vector2f beginning, sf::Vector2f end);
	void sendRespawnMessage();
	void processShot(rapidjson::Value&, int);

private:
	Network network;

	Map gameMap;

	Chat gameChat;	

	HUD hud;

	int timeFromServer;

	// Main game clock
	sf::Time mainClock;

	sf::RenderWindow gameWindow;
	sf::View gameView;

	// Timestep
	float dt;
	float currentTime;
	float accumulator;
	sf::Clock clock;

	sf::Vector2f currentState;
	sf::Vector2f previousState;

	// Player. Must be declared AFTER currentState (Timestep).
	std::shared_ptr<Player> player;
	std::set<std::unique_ptr<ForegroundObject>> bullets;
	std::map<std::string, std::shared_ptr<Enemy>> enemies;

	bool chatWritingEnabled;
	std::wstring capturedText;
	std::wstring playerID;
	std::wstring playerName;

	// Events Queue
	EventQueue eventQueue;

	//Network buffer
	char networkBuffer[cfg::MAX_SERVER_MSG_SIZE];
	char networkDataPreprocessed[cfg::MAX_SERVER_MSG_SIZE];
};

#endif