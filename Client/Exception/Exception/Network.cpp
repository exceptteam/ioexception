#include "Network.h"

#include <iostream>

Network::Network(sf::IpAddress ipAddress, unsigned int port) {
	sf::Socket::Status status = socket.connect(ipAddress, port);

	if (status != sf::Socket::Done)
		throw ServerConnectionError();
	else
		std::cerr << "Connection - success\n";
}

sf::TcpSocket::Status Network::receive(void *data, std::size_t size, std::size_t &received) {
	auto status = socket.receive(data, size, received);

	if (status != sf::Socket::Done)
		std::cerr << "Error - receive\n";

	return status;
}

sf::TcpSocket::Status Network::send(void *data, std::size_t size) {
	auto status = socket.send(data, size);

	if (status != sf::Socket::Done)
		std::cerr << "Error - send\n";

	return status;
}