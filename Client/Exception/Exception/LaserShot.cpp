#include "LaserShot.h"
#include "AuxFunctions.h"
#include "Character.h"

#include <iostream>

LaserShot::LaserShot(sf::Vector2f playerPos, sf::Vector2f endPos) :
	timeOnScreen(cfg::LASER_TIME_ON_SCREEN)
{
	shape.setPrimitiveType(sf::Lines);
	shape.append(sf::Vertex(endPos, sf::Color::Red));
	shape.append(sf::Vertex(playerPos, sf::Color::Red));
	beginning = playerPos;
	end = endPos;
}

LaserShot::LaserShot(sf::Vector2f playerPos, sf::Vector2f mousePos, Map* mapPtr) :
	timeOnScreen(cfg::LASER_TIME_ON_SCREEN)
{
	//check every tile on the way, set end of the line on nearest collision
	sf::Vector2f v = 4.0f*normalize(mousePos);
	sf::Vector2f endPos = playerPos + float(cfg::PLAYER_RADIUS + 1)*normalize(mousePos);
	beginning = endPos;
	while ((endPos.x > 0) && (endPos.y > 0) && (endPos.x < cfg::MAP_WIDTH*cfg::TILE_SIZE)
		&& (endPos.x < cfg::MAP_HEIGHT*cfg::TILE_SIZE)) {
		if (mapPtr->isTileColiding(int(endPos.x / cfg::TILE_SIZE), int(endPos.y / cfg::TILE_SIZE), endPos, 5.0f))
			break;
		for (auto it = mapPtr->characters.begin(); it != mapPtr->characters.end(); ++it) {
			if ((!it->second->getDead()) && (length(endPos - it->second->getPosition()) < cfg::PLAYER_RADIUS))
				goto endWhile;
		}
		endPos += v;
	}
	endWhile:
	shape.setPrimitiveType(sf::Lines);
	shape.append(sf::Vertex(endPos, sf::Color::Red));
	shape.append(sf::Vertex(playerPos, sf::Color::Red));
	end = endPos;
}

bool LaserShot::update(float dt) {
	timeOnScreen -= dt;
	return timeOnScreen > 0;
}

void LaserShot::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	// draw shape
	target.draw(shape, states);
}

