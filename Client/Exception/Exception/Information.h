#ifndef _Information_h
#define _Information_h

#include <string>

#include "Character.h"

class Information
{
public:
	Information() {};
private:
};

class MessageInformation : public Information {
public:
	MessageInformation();
	MessageInformation(std::wstring, std::wstring);
	std::wstring getAuthor() { return author; };
	std::wstring getMessage() { return message; };
private:
	std::wstring author;
	std::wstring message;
};

class CharacterUpdateInformation : public Information {
public:
	CharacterUpdateInformation();
	CharacterUpdateInformation(std::string, cfg::CharacterDirection, sf::Vector2f, bool);
	CharacterUpdateInformation(std::string, cfg::CharacterDirection, sf::Vector2f, bool, float);
	std::string getId() { return id; };
	cfg::CharacterDirection getDirection() { return direction; };
	sf::Vector2f getPosition() { return position; };
	bool getDead() { return dead; };
	float getAngle() { return angle; };
private:
	std::string id;
	cfg::CharacterDirection direction;
	sf::Vector2f position;
	bool dead;
	float angle;
};

//received only once, when joining a game or another player joined
class CharacterSpawnInformation : public Information {
public:
	CharacterSpawnInformation() : id(""), name(""), position(10, 10), dead(false) {};
	CharacterSpawnInformation(std::string id, std::string name, sf::Vector2f pos, bool dead) : id(id), position(pos), name(name), dead(dead) {};
	std::string getId() { return id; };
	std::string getName() { return name; };
	sf::Vector2f getPosition() { return position; };
	bool getDead() { return dead; };

private:
	std::string id;
	std::string name;
	sf::Vector2f position;
	bool dead;
};

class HudUpdateInformation : public Information {
public:
	HudUpdateInformation(unsigned int healthAmt, unsigned int kills, unsigned int deaths) : 
		healthAmt(healthAmt),
		kills(kills),
		deaths(deaths) {};

	unsigned int getHealthAmt() { return healthAmt; }
	unsigned int getKills() { return kills; }
	unsigned int getDeaths() { return deaths; }
private:
	unsigned int healthAmt, kills, deaths;
};


class PlayerHpUpdateInformation : public Information {
public:
	PlayerHpUpdateInformation(unsigned int health_amt) : health_amt(health_amt) {};
	unsigned int getHealthAmt() { return health_amt; }
private:
	unsigned int health_amt;
};

class ShotInformation : public Information {
public:
	ShotInformation(float fx, float fy, float tx, float ty) : begX(fx), begY(fy), endX(tx), endY(ty) {};
	float getBegX() { return begX; };
	float getBegY() { return begY; };
	float getEndX() { return endX; };
	float getEndY() { return endY; };
private:
	float begX, begY, endX, endY;
};

#endif