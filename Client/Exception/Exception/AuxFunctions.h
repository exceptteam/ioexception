#ifndef _AuxFunctions_h
#define _AuxFunctions_h

#include <SFML/Graphics.hpp>

#include <math.h>
#include "Map.h"
#include "Config.h"

#include <codecvt>
#include <string>

/* 
	convert UTF-8 string to wstring
*/
std::wstring utf8_to_wstring(const std::string& str);

/*
	convert wstring to UTF-8 string
*/
std::string wstring_to_utf8(const std::wstring& str);

/*
	Returns a new sf::Vectro2f with values rounded to the nearest integers.
*/
sf::Vector2f round(sf::Vector2f vec);

/*
	Returns a normalized vector from sf::Vector2f
*/
sf::Vector2f normalize(sf::Vector2f vec);

/*
Returns length of a given vector
*/
float length(sf::Vector2f);

/*
Compares string with wstring.
*/
bool compare_s_ws(std::string, std::wstring);

/*
Translates int to cfg::CharacterDirection
*/
cfg::CharacterDirection getDirection(int i);
#endif