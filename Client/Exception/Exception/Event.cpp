#include "Event.h"

Event::Event() {
	type = EmptyEvent;
	timestamp = sf::milliseconds(0);
	std::shared_ptr<Information> info(new Information());
	information = info;
}

Event::Event(sf::Time t, EventType eType, std::shared_ptr<Information> info) {
	type = eType;
	timestamp = t;
	information = info;
}

sf::Time Event::getTimestamp() const {
	return timestamp;
}