#include "Character.h"
#include "AuxFunctions.h"

Character::Character(sf::Vector2f initPos, Map* mPtr, bool isDead) : dead(isDead) {
	if (!playerTexture.loadFromFile(cfg::PLAYER_TEXTURE_PATH)) {
		// error...
	}

	angle = 0;
	mapPtr = mPtr;

	shape = sf::Sprite(playerTexture, sf::IntRect(0, 0, 50, 50));
	shape.setOrigin(sf::Vector2f(shape.getLocalBounds().height / 2, shape.getLocalBounds().width / 2));
	shape.setPosition(initPos);
	shape.scale(1.4f, 1.4f);
	shape.setColor(sf::Color::Yellow);

	circ = sf::CircleShape(cfg::PLAYER_RADIUS);
	circ.setOrigin(sf::Vector2f(circ.getLocalBounds().height / 2, circ.getLocalBounds().width / 2));
	circ.setPosition(initPos);

	directions = std::vector<bool>(cfg::DIRECTIONS_AMT, false);
}

void Character::setDirection(Direction direction, bool isMoving) {
	directions[direction] = isMoving;
}

void Character::setDirection(cfg::CharacterDirection dir) {
	for (auto it = directions.begin(); it != directions.end(); ++it)
		*it = false;

	if (dir == cfg::CharacterDirection::North || dir == cfg::CharacterDirection::NE || dir == cfg::CharacterDirection::NW) {
		directions[Character::Direction::UP] = true;
	}
	if (dir == cfg::CharacterDirection::East || dir == cfg::CharacterDirection::SE || dir == cfg::CharacterDirection::NE) {
		directions[Character::Direction::RIGHT] = true;
	}
	if (dir == cfg::CharacterDirection::South || dir == cfg::CharacterDirection::SE || dir == cfg::CharacterDirection::SW) {
		directions[Character::Direction::DOWN] = true;
	}
	if (dir == cfg::CharacterDirection::West || dir == cfg::CharacterDirection::SW || dir == cfg::CharacterDirection::NW) {
		directions[Character::Direction::LEFT] = true;
	}
}


bool Character::tryToSetCharacterPos(sf::Vector2f & pos) {
	if (detectCollision(pos)) {
		//pos = round(shape.getPosition());
		pos = shape.getPosition();
		return false;
	}
	else {
		shape.setPosition(round(pos));
//		shape.setPosition(pos);
		circ.setPosition(pos);
		return true;
	}
}

bool Character::detectCollision(sf::Vector2f futurePos) {
	/*
	Assumptions:
	Tile size is 32x32
	Player radious is 25px

	Imposible to move more than one tile in one frame
	Currently true, bc in player.update()
	const dt = 0.015f
	const SPEED = 400.f
	so max movement 400 * 0.015 = 6px

	Tiles outside map limits, are not reachable map is surrounded by blocking tiles.
	*/

	sf::Vector2i topLeft(static_cast<int>(futurePos.x), static_cast<int>(futurePos.y));

	// Finds the neareast top left corner of a tile.
	topLeft.x = topLeft.x - (topLeft.x % cfg::TILE_SIZE);
	topLeft.y = topLeft.y - (topLeft.y % cfg::TILE_SIZE);

	// Move one tile up and one tile left.
	topLeft -= sf::Vector2i(cfg::TILE_SIZE, cfg::TILE_SIZE);

	// Asures top left corner is inside the map limits.
	topLeft.x = std::max(topLeft.x, 0);
	topLeft.y = std::max(topLeft.y, 0);

	// Iterates through tileset (3 tiles x  3 tiles), checks whether tile is blocking-type 
	// and if it is checks whether this tile collides with futerePos.  
	auto check3x3tiles = [=](int reserve) {
		sf::Vector2i tmp;

		for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++) {
			tmp = topLeft + sf::Vector2i(i * cfg::TILE_SIZE, j * cfg::TILE_SIZE);

			unsigned int x = tmp.x / cfg::TILE_SIZE;
			unsigned int y = tmp.y / cfg::TILE_SIZE;

			if (mapPtr->isTileColiding(x, y, futurePos, cfg::PLAYER_RADIUS + reserve))
				return true;
		}
		return false;
	};

	//checking collision with tiles
	if (check3x3tiles(0))
		return true;

	// Check collisions with other players
	for (auto it = mapPtr->characters.begin(); it != mapPtr->characters.end(); ++it) {
		if ((it->first != id) && (!it->second->getDead()) &&
		(length(it->second->getPosition() - getPosition()) < 2 * cfg::PLAYER_RADIUS)) {
			futurePos = round(getPosition() + 1.f*normalize(getPosition() - it->second->getPosition()));
			if (!check3x3tiles(2)) {
				shape.setPosition(futurePos);
				circ.setPosition(futurePos);
			}
			return true;
		}
	}

	return false;
}


sf::Vector2f Character::getPosition() {
	return shape.getPosition();
}

void Character::setAngle(float ang) {
	angle = ang;
}

float Character::getAngle() {
	return angle;
}

void Character::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	// apply the transform
	states.transform *= getTransform();

	// apply the tileset texture
	states.texture = &playerTexture;

	// draw shape
//	target.draw(circ, states);
	target.draw(shape, states);
}

void Character::getResultants(ResultantVer & resultantVer, ResultantHor & resultantHor) {
	resultantVer = ResultantVer::NONE;
	resultantHor = ResultantHor::NONE;

	if (directions[Direction::UP] && !directions[Direction::DOWN])
		resultantVer = ResultantVer::NORTH;
	else if (!directions[Direction::UP] && directions[Direction::DOWN])
		resultantVer = ResultantVer::SOUTH;

	if (directions[Direction::LEFT] && !directions[Direction::RIGHT])
		resultantHor = ResultantHor::WEST;
	else if (!directions[Direction::LEFT] && directions[Direction::RIGHT])
		resultantHor = ResultantHor::EAST;
}

sf::Vector2f Character::move(float dt) {
	shape.setRotation(angle);
	 
	ResultantVer resultantVer;
	ResultantHor resultantHor;
	getResultants(resultantVer, resultantHor);

	//Player is not moving
	if (resultantHor == ResultantHor::NONE && resultantVer == ResultantVer::NONE)
		return circ.getPosition();
	// Player moves horizontally or vertically
	else if (resultantHor == ResultantHor::NONE || resultantVer == ResultantVer::NONE) {
		sf::Vector2f velocity = sf::Vector2f(static_cast<int>(resultantHor)* cfg::PLAYER_SPEED,
											 static_cast<int>(resultantVer) * cfg::PLAYER_SPEED);

		sf::Vector2f current = circ.getPosition();
		current += velocity * dt;

		tryToSetCharacterPos(current);
	}
	// Player moves diagonally
	else {
		sf::Vector2f velocity = sf::Vector2f(static_cast<int>(resultantHor)* cfg::PLAYER_SPEED_DIAGONALLY,
											 static_cast<int>(resultantVer) * cfg::PLAYER_SPEED_DIAGONALLY);

		sf::Vector2f current = circ.getPosition();
		current += velocity * dt;

		// Try to move diagonally
		if (tryToSetCharacterPos(current))
			return current;

		// Try to move horizontally
		velocity = sf::Vector2f(0, static_cast<int>(resultantVer) * cfg::PLAYER_SPEED);

		current = circ.getPosition();
		current += velocity * dt;

		if (tryToSetCharacterPos(current))
			return current;

		// Try to move vertically
		velocity = sf::Vector2f(static_cast<int>(resultantHor) * cfg::PLAYER_SPEED, 0);

		current = circ.getPosition();
		current += velocity * dt;

		if (tryToSetCharacterPos(current))
			return current;

	}
	return shape.getPosition();
}

void Character::setPos(sf::Vector2f pos) {
	shape.setPosition(pos);
	circ.setPosition(pos);
}

void Character::setDead(bool d) {
	if (d && !dead) { //just died
		dead = true;
		for_each(directions.begin(), directions.end(), [](bool val){ 
			val = false;
		});
		//change texture
		shape.setTextureRect(sf::IntRect(96, 336, 48, 48));
	}
	else if (dead && !d) { //respawn
		dead = false;
		//change texture
		shape.setTextureRect(sf::IntRect(0, 0, 50, 50));
	}
}