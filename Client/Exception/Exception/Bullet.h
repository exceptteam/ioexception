#ifndef _Bullet_h
#define _Bullet_h

#include <SFML/Graphics.hpp>
#include <memory>

#include "Map.h"
#include "ForegroundObject.h"
#include "Config.h"

class Bullet : public ForegroundObject {
public:
	Bullet(sf::Vector2f position,
		sf::Vector2f shift,
		float range,
		Map* mapPtr,
		std::string shooterID);
	bool update(float dt);
	sf::Vector2f getPos() { return shape.getPosition(); };

private:
	virtual void draw(sf::RenderTarget&, sf::RenderStates) const;

private:
	sf::CircleShape shape;
	float range;
	sf::Vector2f shift;
	Map* mapPtr;
	std::string shooterID;
};

#endif