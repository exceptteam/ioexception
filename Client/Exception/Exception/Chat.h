#ifndef _Chat_h
#define _Chat_h

#include <deque>
#include <list>
#include <string>

#include <SFML/Graphics.hpp>

#include "Message.h"
#include "Config.h"

class Chat
{
public:
	Chat();
	~Chat();
	Message getLastMessage();
	void addMessage(Message);
	void removeMessages(int n);
	void removeAllMessages();
	sf::Text getVisibleMessage(sf::Vector2f, std::wstring text);
	const std::list<sf::Text> & Chat::getVisibleMessages(sf::Vector2f position);
	void update(float dt);

private:
	const float MESSAGE_DISTANCE = 15.f;
	const float TIME_ON_SCREEN = 5.f;
	unsigned int MAX_ON_SCREEN = 5;
	unsigned int CHARACTER_SIZE = 15;

	void Chat::addVisibleMessage(Message msg);

	std::deque<Message> gameLog;
	sf::Font font;
	sf::Text currentMessage;

	std::list<sf::Text> visibleMessages;
	std::list<float> timeLeft;
};

#endif