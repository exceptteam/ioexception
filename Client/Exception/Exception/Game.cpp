#include "Game.h"
#include "AuxFunctions.h"
#include "LaserShot.h"
#include "CollisionFunctions.h"

#include <memory>
#include <vector>
#include <fstream>
#include <cstring>

void Game::DEBUG_CHAT_EVENT(unsigned int wait) {
	sf::Time delay = sf::milliseconds(wait);
	sf::Time eventTime = mainClock + delay;
	std::shared_ptr<MessageInformation> ptr(new MessageInformation(L"DEBUG", L"Tak to testowa wiadomosc"));
	Event testMessage(eventTime, ChatEvent, ptr);
	eventQueue.addEvent(testMessage);
}

void Game::DEBUG_REMOVE_HIT_POINT(unsigned int wait) {
	sf::Time delay = sf::milliseconds(wait);
	sf::Time eventTime = mainClock + delay;

	auto health_amt = player->getHealthAmt();

	// Check for overflow
	if (health_amt - 1 < health_amt)
		health_amt--;

	auto ptr = std::make_shared<PlayerHpUpdateInformation>(health_amt);

	Event spawnEvent(eventTime, PlayerHpUpdateEvent, ptr);
	eventQueue.addEvent(spawnEvent);
}

void Game::DEBUG_DEATH() {
	player->setDead(!player->getDead());
}

//Game::Game() : 
//	network(Network(cfg::SERVER_ADRESS, cfg::SERVER_PORT)),
//	gameWindow(sf::VideoMode(cfg::WINDOW_WIDTH, cfg::WINDOW_HEIGHT), cfg::WINDOW_TITLE),
//	gameView(sf::FloatRect(0, 0, cfg::WINDOW_WIDTH, cfg::WINDOW_HEIGHT)),
//	currentState(sf::Vector2f(64, 64)),
//	chatWritingEnabled(false),
//	playerID(L"Player")
//{
//	std::cout << cfg::SERVER_ADRESS << " " << cfg::SERVER_PORT << std::endl;
//	std::wcout << playerID << std::endl;
//	loadMapFromServer();
//
//	mainClock = sf::milliseconds(10);
//	player = std::make_shared<Player>(currentState, &gameMap, false);
//	previousState = currentState;
//
//	dt = 0.01f;
//	accumulator = 0.f;
//}

Game::Game(std::string serverIP, std::wstring playerName) :
	network(Network(serverIP, cfg::SERVER_PORT)),
	gameWindow(sf::VideoMode(cfg::WINDOW_WIDTH, cfg::WINDOW_HEIGHT), cfg::WINDOW_TITLE),
	gameView(sf::FloatRect(0, 0, cfg::WINDOW_WIDTH, cfg::WINDOW_HEIGHT)),
	currentState(sf::Vector2f(64, 64)),
	chatWritingEnabled(false),
	playerID(L""),
	playerName(playerName)
	
{
	std::cout << serverIP << " " << cfg::SERVER_PORT << std::endl;
	std::wcout << playerID << std::endl;
	loadMapFromServer();

	mainClock = sf::milliseconds(10);
	player = std::make_shared<Player>(currentState, &gameMap, false);
	previousState = currentState;

	dt = 0.01f;
	accumulator = 0.f;
}

void Game::loadMapFromFile() {
	mapArray level;

	std::string line;
	std::ifstream myfile("map_serial.txt");
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			std::vector<int> row;
			for (int i = 0; i < cfg::MAP_WIDTH; i++)
				row.push_back(line[i]);
			level.push_back(row);
		}
		myfile.close();
	}
	else {
		std::cerr << "Unable to open file\n";
		int tmp;
		std::cin >> tmp;
		exit(1);
	}

	gameMap.load(cfg::TILE_TEXTURE_PATH, level);
}

void Game::loadMapFromServer() {
	mapArray level;

	std::string playerNameStr = wstring_to_utf8(playerName);

	std::string init_msg = "{'Command' : 'JoinGame', 'Player' : {'Name' : '" + playerNameStr + "'}}<EOF>";
	size_t init_size = init_msg.size();

	// Send initial msg to server
	//char init_msg[] = "{'Command' : 'JoinGame',	'Player' : {'Name' : 'TestPlayer'}}<EOF>";
	//size_t init_size = strlen(init_msg);

	if (network.send(const_cast<char*>(init_msg.data()), init_size) != sf::Socket::Done) {
		std::cerr << "Unable to send init msg to server\n";
		std::cin.get();
		exit(1);
	}

	// Receive map from server 
	const size_t size = cfg::MAP_HEIGHT * cfg::MAP_WIDTH;
	size_t received;
	char data[size];

	if (network.receive(data, size, received) == sf::Socket::Done) {
		for (size_t i = 0; i < cfg::MAP_HEIGHT; i++) {
			std::vector<int> row;
			for (size_t j = 0; j < cfg::MAP_WIDTH; j++)
				row.push_back(data[i * cfg::MAP_HEIGHT + j]);
			level.push_back(row);
		}
	}
	else {
		std::cerr << "Unable to get map from server\n";
		std::cin.get();
		exit(1);
	}

	// Load map
	gameMap.load(cfg::TILE_TEXTURE_PATH, level);
}

void Game::run() {
	int counter = 0;
	while (gameWindow.isOpen()) {
		processEvents();
		sendEvents();
		getServerEvents();
		processServerEvents();
		update();
		render();
	}
}

void Game::sendEvents() {
	// This update must be called only once for one cycle of game loop!.
	player->updateDirection();

	//If direction didn't change don't send MoveStart command to server.
	if (!player->getDirectionChanged())
		return;

	// Get DirectonString from player.
	std::string direction = player->getDirectionString().data();
	
	// Empty object must be parsed before we can add our own objects and values.
	rapidjson::Document document;
	char json_empty[] = "{  }";
	document.Parse<0>(json_empty);
	
	document.AddMember("Command", "MoveStart", document.GetAllocator());
	document.AddMember("Direction", "", document.GetAllocator());

	// This string cannot be moved up into "". I don't why it doesn't work. Just deal with it. 
	document["Direction"].SetString(direction.data(), direction.size(), document.GetAllocator());

	//Convert JSON document to string
	rapidjson::StringBuffer strbuf;
	rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
	document.Accept(writer);

	std::string playerDirectionMsg = strbuf.GetString();
	playerDirectionMsg += "<EOF>";
	
	size_t size = playerDirectionMsg.size();

	network.send((void*)playerDirectionMsg.data() , size);
}

void Game::sendChatMessage(std::wstring capturedText) {
	// Translate wstring to string
	std::string message = wstring_to_utf8(capturedText);

	//std::cout << message << std::endl;
	rapidjson::Document document;
	char json_empty[] = "{  }";
	document.Parse<0>(json_empty);

	document.AddMember("Command", "ChatMessage", document.GetAllocator());
	document.AddMember("Message", "", document.GetAllocator());

	document["Message"].SetString(message.data(), message.size(), document.GetAllocator());

	rapidjson::StringBuffer strbuf;
	rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
	document.Accept(writer);

	std::string chatMsg = strbuf.GetString();
	chatMsg += "<EOF>";
	size_t size = chatMsg.size();

	network.send((void*)chatMsg.data(), size);
}

void Game::sendShotInfo(sf::Vector2f beginning, sf::Vector2f end) {
	rapidjson::Document document;
	char json_empty[] = "{  }";
	document.Parse<0>(json_empty);

	document.AddMember("Command", "ShotInfo", document.GetAllocator());
	document.AddMember("begX", beginning.x / cfg::TILE_SIZE, document.GetAllocator());
	document.AddMember("begY", beginning.y / cfg::TILE_SIZE, document.GetAllocator());
	document.AddMember("endX", end.x/cfg::TILE_SIZE, document.GetAllocator());
	document.AddMember("endY", end.y/cfg::TILE_SIZE, document.GetAllocator());

	rapidjson::StringBuffer strbuf;
	rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
	document.Accept(writer);

	std::string shotInfoMsg = strbuf.GetString();
	shotInfoMsg += "<EOF>";
	size_t size = shotInfoMsg.size();
	network.send((void*)shotInfoMsg.data(), size);
}

void Game::sendRespawnMessage() {
	rapidjson::Document document;
	char json_empty[] = "{  }";
	document.Parse<0>(json_empty);

	document.AddMember("Command", "ReadyToRespawn", document.GetAllocator());

	rapidjson::StringBuffer strbuf;
	rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
	document.Accept(writer);

	std::string RespawnMsg = strbuf.GetString();
	RespawnMsg += "<EOF>";
	size_t size = RespawnMsg.size();
	network.send((void*)RespawnMsg.data(), size);
}

void Game::processEvents() {
	sf::Event event;

	while (gameWindow.pollEvent(event)) {
		if (event.type == sf::Event::Closed)
			gameWindow.close();
		
		if (event.type == sf::Event::MouseMoved)
			handleMouseMove(event);

		if (chatWritingEnabled) {
			if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Return) {
				sendChatMessage(capturedText);
				chatWritingEnabled = false;
				capturedText.clear();
				return;
			}
			// We don't want to capture CR character (Return key)
			if (event.type == sf::Event::TextEntered && event.text.unicode != 13) {
				// Backspace character
				if (event.text.unicode == 8)
					capturedText.pop_back();
				else
					capturedText += event.text.unicode;
			}
		}
		else {
		if (event.type == sf::Event::KeyPressed)
			handlePressedKey(event);
		}
		
		if (event.type == sf::Event::KeyReleased)
			handleReleasedKey(event);

		if (event.type == sf::Event::MouseButtonPressed)
			handleMouseButtonPressed(event);
	}
}

void Game::handleMouseMove(sf::Event event) {
	// set player's direction (rotation)
	sf::Vector2f middle = sf::Vector2f(gameWindow.getSize().x / 2, gameWindow.getSize().y / 2);
	sf::Vector2f direction = sf::Vector2f(event.mouseMove.x, event.mouseMove.y) - middle;
	float angle = atan2(direction.y, direction.x) * 180 / cfg::PI;
	player->setAngle(angle);
}

void Game::handlePressedKey(sf::Event event) {
	if (event.key.code == sf::Keyboard::Return) {
		chatWritingEnabled = true;
		return;
	}

	if (event.key.code == sf::Keyboard::W)
		player->setDirection(Player::Direction::UP, true);

	if (event.key.code == sf::Keyboard::S)
		player->setDirection(Player::Direction::DOWN, true);

	if (event.key.code == sf::Keyboard::A)
		player->setDirection(Player::Direction::LEFT, true);

	if (event.key.code == sf::Keyboard::D)
		player->setDirection(Player::Direction::RIGHT, true);


	// For debuggin for now. Pressing K causes the game to receive information about a chat message 100ms from now.
	if (event.key.code == sf::Keyboard::K)
		DEBUG_CHAT_EVENT(100);

	// Removes one hit point from player if possible
	if (event.key.code == sf::Keyboard::H)
		DEBUG_REMOVE_HIT_POINT(50);

	// Player's death
	if (event.key.code == sf::Keyboard::Y)
		DEBUG_DEATH();
}

void Game::handleReleasedKey(sf::Event event) {

	if (event.key.code == sf::Keyboard::W)
		player->setDirection(Player::Direction::UP, false);

	if (event.key.code == sf::Keyboard::S)
		player->setDirection(Player::Direction::DOWN, false);

	if (event.key.code == sf::Keyboard::A)
		player->setDirection(Player::Direction::LEFT, false);

	if (event.key.code == sf::Keyboard::D) 
		player->setDirection(Player::Direction::RIGHT, false);
}

void Game::handleMouseButtonPressed(sf::Event event) {
	if (event.key.code == sf::Mouse::Button::Left) {
		if (player->getGunCooldown() > 0)
			return;
		sf::Vector2f middle = sf::Vector2f(gameWindow.getSize().x / 2, gameWindow.getSize().y / 2);
		sf::Vector2f mouse = sf::Vector2f(event.mouseButton.x, event.mouseButton.y);
		sf::Vector2f direction = mouse - middle;

		auto tmp = player->fireGun(direction, true);
		if (tmp != nullptr) {
			sendShotInfo(tmp->getBeginning(), tmp->getPos());
			bullets.insert(std::move(tmp));
		}
	}
	if (event.key.code == sf::Mouse::Button::Right) {
		if (player->getDead())
			sendRespawnMessage();
	}
}


void Game::update() {
	// algorithm from http://gafferongames.com/game-physics/fix-your-timestep/

	float deltaTime = clock.getElapsedTime().asSeconds();
	sf::Time lapse = clock.restart();

	if (timeFromServer != 0) {
		mainClock = 0.3f * (mainClock + lapse) + 0.7f * sf::milliseconds(timeFromServer);
		timeFromServer = 0;
	}
	else {
		mainClock += lapse;
	}

	if (deltaTime > 0.25f)
		deltaTime = 0.25f;

	accumulator += deltaTime;

	while (accumulator >= dt)
	{
		accumulator -= dt;
		previousState = currentState;
		player->update(dt);
		currentState = player->getPosition();
		for (auto it = enemies.begin(); it != enemies.end(); ++it) {
			it->second->update(dt);
		}
		for (auto it = bullets.begin(); it != bullets.end(); ) {
			if (!(*it)->update(dt))
				bullets.erase(it++);
			else
				++it;
		}
		gameChat.update(dt);
		player->updateGunCooldown(dt);
	}

	float alpha = accumulator / dt;

	// interpolate
	// Version with alpha makes the map moving smoother, but causes the player to oscillate a little
	// Version without makes the player model stay in place, but map is not EXACTLY smooth (barely noticable)
	//sf::Vector2f interpolatedState = currentState;
	sf::Vector2f interpolatedState = currentState * alpha + previousState * (1 - alpha);

	// round
	sf::Vector2f roundedState = round(interpolatedState);
	player->setModelPosition(roundedState);
	
	// view follows player
	gameView.setCenter(roundedState);
}

void Game::render() {
	gameWindow.clear();

	gameWindow.setView(gameView);

	gameWindow.draw(gameMap);

	for (auto it = bullets.begin(); it != bullets.end(); ++it) {
		gameWindow.draw(*(*it));
	}

	auto drawEnemies = [&](){
		//draw dead players
		for (auto it = enemies.begin(); it != enemies.end(); ++it) {
			if (it->second->getDead());
				gameWindow.draw(*it->second);
		}

		//draw alive players
		for (auto it = enemies.begin(); it != enemies.end(); ++it) {
			if (!it->second->getDead());
				gameWindow.draw(*it->second);
		}
	};

	//if player is dead, he should be drawn first
	if (player->getDead()) {
		gameWindow.draw(*player);
		drawEnemies();
	}
	else {
		drawEnemies();
		gameWindow.draw(*player);
	}

	// HUD in the top right corner
	sf::Vector2f hudPosition = gameView.getCenter() + sf::Vector2f(gameView.getSize().x / 2, -gameView.getSize().y / 2);
	hud.setPos(hudPosition);
	gameWindow.draw(hud);


	// Chat in the lower left corner
	sf::Vector2f chatPosition = gameView.getCenter() - sf::Vector2f(gameView.getSize().x / 2, -gameView.getSize().y / 3);
	const std::list<sf::Text> & visibleMessages = gameChat.getVisibleMessages(chatPosition);
	for (auto it = visibleMessages.begin(); it != visibleMessages.end(); ++it)
		gameWindow.draw(*it);


	// Written text in the top left corner
	sf::Vector2f textPosition = gameView.getCenter() - sf::Vector2f(gameView.getSize().x / 2, gameView.getSize().y / 2);
	if (chatWritingEnabled)
		gameWindow.draw(gameChat.getVisibleMessage(textPosition, capturedText));

	gameWindow.display();
}

void Game::getServerEvents() {
	std::size_t size = cfg::MAX_SERVER_MSG_SIZE;
	std::size_t received;
	network.receive(networkBuffer, size, received);

	int operationCode = getPacket(networkBuffer, networkDataPreprocessed);

	// If we didn't find <EOF> in the message - not a proper message
	// NOTE: Buffer is way bigger than messages for now, if it changes, we should improve the code,
	// otherwise there's a possibility we'll lose a packet
	if (operationCode == -1)
		return;

	rapidjson::Document doc;
	doc.Parse<0>(networkDataPreprocessed);

	// If packet is in the wrong format (or corrupted, or something), don't work with it
	if (!doc.IsObject()) {
		return;
	}

	// Timestamp
	if (doc.HasMember("TimeStamp")) {
		rapidjson::Value& timeStamp = doc["TimeStamp"];
		timeFromServer = timeStamp.GetInt();
	}

	// Chat Messages
	if (doc.HasMember("ChatMessages")) {
		rapidjson::Value& chatMessages = doc["ChatMessages"];
		for (rapidjson::SizeType i = 0; i < chatMessages.Size(); ++i) {
			parseChatMessage(chatMessages[i]);
		}
	}

	// Player
	if (doc.HasMember("Player")) {
		rapidjson::Value& playerInfo = doc["Player"];

		float horPosition = playerInfo["X"].GetDouble() * cfg::TILE_SIZE;
		float verPosition = playerInfo["Y"].GetDouble() * cfg::TILE_SIZE;
		bool dead = (playerInfo["CurHp"].GetInt() <= 0);
		unsigned int curHealth = playerInfo["CurHp"].GetUint();

		std::string pid = playerInfo["Id"].GetString();
		playerID = std::wstring(pid.begin(), pid.end());
		
		unsigned int kills = 0;
		if (playerInfo.HasMember("Kills"))
			kills = playerInfo["Kills"].GetUint();

		unsigned int deaths = 0;
		if (playerInfo.HasMember("Deaths"))
			deaths = playerInfo["Deaths"].GetUint();

		// Hud update event
		auto hudUpdateInfo = std::make_shared<HudUpdateInformation>(curHealth, kills, deaths);
		Event hudUpdateEvent(mainClock, HudUpdateEvent, hudUpdateInfo);
		eventQueue.addEvent(hudUpdateEvent);

		//Health update event
		auto healthUpdateInfo = std::make_shared<PlayerHpUpdateInformation>(curHealth);
		Event healthUpdateEvent(mainClock, PlayerHpUpdateEvent, healthUpdateInfo);
		eventQueue.addEvent(healthUpdateEvent);

		// It's literal "Player" for now, since the server doesn't know our name yet.
		// And we believe that info is sent the right way, so we shouldn't even need to check it.
		std::shared_ptr<CharacterUpdateInformation> ptr(new CharacterUpdateInformation(pid, cfg::CharacterDirection::None, sf::Vector2f(horPosition, verPosition), dead));

		// Server doesn't send timestamps yet, so we're using mainClock as an argument for now.
		Event movementEvent(mainClock, CharacterMovementUpdateEvent, ptr);
		eventQueue.addEvent(movementEvent);
	}

	// Enemies
	if (doc.HasMember("Enemies")) {
		rapidjson::Value& enemyPlayers = doc["Enemies"];
		for (auto iter = enemies.begin(); iter != enemies.end(); ++iter) {
			(*iter).second->markAsUpdated(false);
		}
		for (rapidjson::SizeType i = 0; i < enemyPlayers.Size(); ++i) {
			std::string id = enemyPlayers[i]["Id"].GetString();
			std::string name = enemyPlayers[i]["Name"].GetString();
			bool dead = (enemyPlayers[i]["CurHp"].GetInt() <= 0);
			float angle = enemyPlayers[i]["Angle"].GetDouble();

			cfg::CharacterDirection direction = getDirection(enemyPlayers[i]["Direction"].GetInt());

			float x = enemyPlayers[i]["X"].GetDouble() * cfg::TILE_SIZE;
			float y = enemyPlayers[i]["Y"].GetDouble() * cfg::TILE_SIZE;

			sf::Vector2f pos(x, y);

			if (enemies.find(id) == enemies.end()) {
				auto ptr = std::make_shared<CharacterSpawnInformation>(id, name, pos, dead);
				Event spawnEvent(mainClock, CharacterSpawnEvent, ptr);
				eventQueue.addEvent(spawnEvent);
			}
			else {
				auto ptr = std::make_shared<CharacterUpdateInformation>(id, direction, pos, dead, angle);
				Event movementEvent(mainClock, CharacterMovementUpdateEvent, ptr);
				eventQueue.addEvent(movementEvent);
				enemies[id]->markAsUpdated(true);
			}

		}
		for (auto iter = enemies.begin(); iter != enemies.end();) {
			if (!(*iter).second->isUpdated()) {
				enemies.erase(iter++);
			}
			else { ++iter; }
		}
	}

	// Shots
	if (doc.HasMember("Shots")) {
		int timeEvent;
		if (doc.HasMember("TimeStamp")) {
			rapidjson::Value& timeStamp = doc["TimeStamp"];
			timeEvent = timeStamp.GetInt();
		}
		else {
			timeEvent = 0;
		}
		rapidjson::Value& shots = doc["Shots"];
		for (rapidjson::SizeType i = 0; i < shots.Size(); ++i) {
			processShot(shots[i], timeEvent);
		}
	}
}

void Game::processShot(rapidjson::Value& shot, int time) {
	rapidjson::Value& from = shot["From"];
	rapidjson::Value& to = shot["To"];

	float begX = from["X"].GetDouble();
	float begY = from["Y"].GetDouble();
	float endX = to["X"].GetDouble();
	float endY = to["Y"].GetDouble();
	
	std::cout << endX << ' ' << endY << std::endl;

	std::shared_ptr<ShotInformation> ptr(new ShotInformation(begX, begY, endX, endY));
	Event shotEvent(sf::milliseconds(time), ShotEvent, ptr);

	eventQueue.addEvent(shotEvent);
}

void Game::parseChatMessage(rapidjson::Value& chatMessages) {
	std::string playerName = chatMessages["SenderName"].GetString();
	std::string message = chatMessages["Message"].GetString();

	std::wstring wPlayerName = std::wstring(playerName.begin(), playerName.end());
	std::wstring wMessage = utf8_to_wstring(message);
	
	std::shared_ptr<MessageInformation> ptr(new MessageInformation(wPlayerName, wMessage));
	Event messageEvent(mainClock, ChatEvent, ptr);
	eventQueue.addEvent(messageEvent);
}

int Game::getPacket(char* source, char* dest) {
	char eof[] = "<EOF>";
	char *pos = NULL;
	pos = strstr(source, eof);
	if (pos != NULL) {
		size_t size = (pos - source) * sizeof(char);
		strncpy_s(dest, size + 1, source, size);
		return size;
	}
	else 
		return - 1;
}

void Game::processServerEvents() {
	sf::Time timeOfNextEvent = eventQueue.getFirstTime();
	while (mainClock >= timeOfNextEvent && timeOfNextEvent.asMilliseconds() != 0) {
		Event nextEvent = eventQueue.getNextEvent();
		processServerEvent(nextEvent);
		timeOfNextEvent = eventQueue.getFirstTime();
	}
}

void Game::enemyJoined(std::string id, std::string name, sf::Vector2f initPos, bool dead) {
	std::unique_ptr<Enemy> p(new Enemy(id, name, &gameMap, initPos, dead));
	enemies[id] = std::move(p);
	gameMap.characters[id] = enemies[id];
}

void Game::processServerEvent(Event event) {
	EventType eventType = event.getType();
	if (eventType == ChatEvent) {
		std::shared_ptr<MessageInformation> chatInfo = std::static_pointer_cast<MessageInformation>(event.getInformation());
		
		Message msg(chatInfo->getAuthor(), chatInfo->getMessage());
		gameChat.addMessage(msg);
	}
	else if (eventType == CharacterMovementUpdateEvent) {
		std::shared_ptr<CharacterUpdateInformation> enemyInfo = std::static_pointer_cast<CharacterUpdateInformation>(event.getInformation());

		std::string id = enemyInfo->getId();
		cfg::CharacterDirection dir = enemyInfo->getDirection();
		sf::Vector2f position = enemyInfo->getPosition();
		bool dead = enemyInfo->getDead();

		// If the information is about the player
		if (compare_s_ws(id, playerID)) {
			player->setPos(position);
			player->setDead(dead);			
		}
		// For other characters
		else {
			// Make sure that such character exists
			if (enemies.find(id) != enemies.end()) {
				enemies[id]->setPos(position);
				enemies[id]->setDirection(dir);
				enemies[id]->setDead(dead);
				enemies[id]->setAngle(enemyInfo->getAngle());
			}
		}
	}
	else if (eventType == CharacterSpawnEvent) {
		std::shared_ptr<CharacterSpawnInformation> spawnInfo = std::static_pointer_cast<CharacterSpawnInformation>(event.getInformation());

		std::string id = spawnInfo->getId();
		std::string name = spawnInfo->getName();
		sf::Vector2f position = spawnInfo->getPosition();
		bool dead = spawnInfo->getDead();
		
		if (!compare_s_ws(id, playerID))
			enemyJoined(id, name, position, dead);
	}
	else if (eventType == PlayerHpUpdateEvent) {
		std::shared_ptr<PlayerHpUpdateInformation> hpInfo = std::static_pointer_cast<PlayerHpUpdateInformation>(event.getInformation());
		player->setHealthAmt(hpInfo->getHealthAmt());
	}
	else if (eventType == ShotEvent) {
		std::shared_ptr<ShotInformation> shotInfo = std::static_pointer_cast<ShotInformation>(event.getInformation());

		float begX = shotInfo->getBegX() * cfg::TILE_SIZE;
		float begY = shotInfo->getBegY() * cfg::TILE_SIZE;
		float endX = shotInfo->getEndX() * cfg::TILE_SIZE;
		float endY = shotInfo->getEndY() * cfg::TILE_SIZE;


		// This generates lasers in weird directions. 
		std::unique_ptr<ForegroundObject> ptr(new LaserShot(sf::Vector2f(begX, begY), sf::Vector2f(endX, endY)));

		bullets.insert(std::move(ptr));
	}
	else if (eventType == HudUpdateEvent) {
		std::shared_ptr<HudUpdateInformation> hudUpdateInfo = std::static_pointer_cast<HudUpdateInformation>(event.getInformation());
		hud.update(hudUpdateInfo->getHealthAmt(), hudUpdateInfo->getKills(), hudUpdateInfo->getDeaths());
	}
}