#ifndef _LaserShot_h
#define _LaserShot_h

#include <SFML/Graphics.hpp>
#include <memory>

#include "Map.h"
#include "ForegroundObject.h"
#include "Config.h"

class LaserShot : public ForegroundObject {
public:
	LaserShot(sf::Vector2f playerPos,
		sf::Vector2f mousePos);
	LaserShot(sf::Vector2f playerPos,
		sf::Vector2f mousePos,
		Map* mapPtr);
	bool update(float dt);
	sf::Vector2f getPos() { return end; };
	sf::Vector2f getBeginning() { return beginning; };

private:
	virtual void draw(sf::RenderTarget&, sf::RenderStates) const;

	float timeOnScreen;

	sf::VertexArray shape;
	sf::Vector2f end;
	sf::Vector2f beginning;
};

#endif