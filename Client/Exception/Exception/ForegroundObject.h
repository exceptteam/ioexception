#ifndef _ForegroundObject_h
#define _ForegroundObject_h

#include <SFML/Graphics.hpp>

class ForegroundObject: public sf::Drawable, public sf::Transformable {
public:
	virtual bool update(float dt) = 0;
	virtual sf::Vector2f getPos() = 0;
	virtual sf::Vector2f getBeginning() { return sf::Vector2f(0, 0); };
};

#endif