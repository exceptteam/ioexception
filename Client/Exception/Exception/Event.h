#ifndef _Event_h
#define _Event_h

#include <SFML/System.hpp>
#include <memory>

#include "Information.h"

enum EventType {
	EmptyEvent = 0,
	ChatEvent = 1,
	OpponentShootEvent = 2,
	CharacterMovementUpdateEvent = 3,
	OpponentDeathEvent = 4,
	CharacterSpawnEvent = 5,
	PlayerSpawnEvent = 6,
	PlayerHpUpdateEvent = 7,
	ShotEvent = 8,
	HudUpdateEvent = 9
};

class Event
{
public:

	Event();
	Event(sf::Time t, EventType, std::shared_ptr<Information>);
	sf::Time getTimestamp() const;
	EventType getType() { return type; };
	std::shared_ptr<Information> getInformation() { return information; };
	
private:
	sf::Time timestamp;
	EventType type;
	std::shared_ptr<Information> information;
};

#endif