#include "EventQueue.h"
#include <iostream>

EventQueue::EventQueue() {

}

Event EventQueue::getNextEvent() {
	if (mQueue.size() == 0) {
		std::shared_ptr<Information> ptr(new Information());
		return Event(sf::milliseconds(0), EmptyEvent, ptr);
	}
	else {
		Event ret = *(mQueue.begin());
		mQueue.erase(mQueue.begin());
		return ret;
	}
}

void EventQueue::addEvent(Event event) {
	mQueue.insert(event);
}

sf::Time EventQueue::getFirstTime() {
	if (mQueue.size() == 0) {
		sf::Time time = sf::milliseconds(0);
		return time;
	} else {
		Event firstEvent = *(mQueue.begin());
		return firstEvent.getTimestamp();
	}
}