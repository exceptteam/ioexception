#ifndef _Tile_h
#define _Tile_h

#include <SFML/Graphics.hpp>


class Tile {
public:
	enum TileDirection
	{
		Up = 0,
		Right = 1,
		Down = 2,
		Left = 3
	};

	enum TileType
	{
		RoomFloor = 0,
		Groung = 1,
		Road = 2,
		RoadEdge = 3,
		RoadTurn = 4,
		Wall = 5,
		WallCorner = 6,
		WideWall = 7,
		WideWallCorner = 8,
		Grass = 9,
		Door = 10,
		RoadDelimiter = 11,//road with traffic lane markings
		SmallCorner = 12
	};

	Tile(TileDirection tileDirection, sf::Vector2f topLeftCorner);

	virtual bool isColiding(const sf::Vector2f playerPos, const float radius) {
		return false;
	}

protected:
	TileType type;
	TileDirection tileDirection;
	sf::Vector2f topLeftCorner;

	const static unsigned int TILE_DIRECTION_AMT = 4;

	struct rect {
		sf::Vector2f topLeftCorner;
		float horizontalSide;
		float verticalSide;
	};
};


class WideWall : public Tile {
	const float SIDE = 32.f;
	rect rects[TILE_DIRECTION_AMT];

public:
	WideWall(TileDirection tileDirection, sf::Vector2f topLeftCorner);

	virtual bool isColiding(const sf::Vector2f playerPos, const float radius);
};


class Wall : public Tile {
	const float LONG_SIDE = 32.f;
	const float SHORT_SIDE = 16.f;
	rect rects[TILE_DIRECTION_AMT];

public:
	Wall(TileDirection tileDirection, sf::Vector2f topLeftCorner);

	virtual bool isColiding(const sf::Vector2f playerPos, const float radius);
};


class WallCorner : public Tile {
	const float LONG_SIDE = 32.f;
	const float SHORT_SIDE = 16.f;
	rect rects[TILE_DIRECTION_AMT][2];
	
public:
	WallCorner(TileDirection tileDirection, sf::Vector2f topLeftCorner);
	virtual bool isColiding(const sf::Vector2f playerPos, const float radius);
};


class WideWallCorner : public Tile {
	const float SIDE = 32.f;
	rect rects[TILE_DIRECTION_AMT]; 

public:
	WideWallCorner(TileDirection tileDirection, sf::Vector2f topLeftCorner);
	virtual bool isColiding(const sf::Vector2f playerPos, const float radius);
};

class SmallCorner : public Tile {
	const float SIDE = 16.f;
	rect rects[TILE_DIRECTION_AMT];
public:
	SmallCorner(TileDirection tileDirection, sf::Vector2f topLeftCorner);
	virtual bool isColiding(const sf::Vector2f playerPos, const float radius);
};

#endif