#include "Message.h"


Message::Message() : 
	author(L""), 
	messageText(L"")
{
}

Message::Message(std::wstring auth, std::wstring msg) : author(auth), messageText(msg)
{
}

Message::~Message()
{
}

std::wstring Message::getAuthor() {
	return author;
}

std::wstring Message::getMessage() {
	return  messageText;
}

void Message::setMessage(std::wstring str) {
	messageText = str;
}
