#include "Information.h"

MessageInformation::MessageInformation() : author(L""), message(L"") {};

MessageInformation::MessageInformation(std::wstring aut, std::wstring msg) : author(aut), message(msg) {};

CharacterUpdateInformation::CharacterUpdateInformation() : id("-1"), direction(cfg::CharacterDirection::None),  position(-10, -10) {};

CharacterUpdateInformation::CharacterUpdateInformation(std::string id, cfg::CharacterDirection dir, sf::Vector2f pos, bool dead, float angle) : 
	id(id), direction(dir), position(pos), dead(dead), angle(angle)  {};

CharacterUpdateInformation::CharacterUpdateInformation(std::string id, cfg::CharacterDirection dir, sf::Vector2f pos, bool dead) :
CharacterUpdateInformation(id, dir, pos, dead, 0) {};