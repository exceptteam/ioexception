#include"Bullet.h"
#include"AuxFunctions.h"
#include"Character.h"

Bullet::Bullet(sf::Vector2f position, sf::Vector2f shift, float range, Map* mapPtr, std::string shooterID) : 
	range(range),
	shift(shift),
	mapPtr(mapPtr), 
	shooterID(shooterID)
{
	shape.setRadius(5);
	shape.setFillColor(sf::Color(255, 0, 0));
	shape.setPosition(position+normalize(shift)*(cfg::PLAYER_RADIUS+1));
}

bool Bullet::update(float dt) {
	sf::Vector2f pos = shape.getPosition();
	pos += dt*shift;
	range -= dt;
	shape.setPosition(pos);
	if (mapPtr->isTileColiding(std::max(int(pos.x / cfg::TILE_SIZE), 0), std::max(int(pos.y / cfg::TILE_SIZE), 0), pos, 5.0f))
		return false;
	for (auto it = mapPtr->characters.begin(); it != mapPtr->characters.end(); ++it) {
		if ((length(shape.getPosition() - it->second->getPosition()) < float(5 + cfg::PLAYER_RADIUS)) && (it->second->getID() != shooterID) && (!it->second->getDead()))
			return false;
	}
	return range > 0;
}

void Bullet::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	// draw shape
	target.draw(shape, states);
}