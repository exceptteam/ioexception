#include "HUD.h"

HUD::HUD() :
	backGround(sf::Vector2f(cfg::HEALTH_BAR_WIDTH, cfg::HEALTH_BAR_HEIGTH)),
	foreGround(sf::Vector2f(0, cfg::HEALTH_BAR_HEIGTH))
{
	if (!font.loadFromFile(cfg::FONT_PATH)) {
		// error..
	}

	backGround.setFillColor(sf::Color::Green);
	backGround.setOutlineColor(sf::Color::Black);
	backGround.setOutlineThickness(cfg::HEALTH_BAR_OUTLINE);
	foreGround.setFillColor(sf::Color::Red);

	killsAmt.setCharacterSize(cfg::CHARACTER_SIZE);
	killsAmt.setFont(font);
	deathsAmt.setCharacterSize(cfg::CHARACTER_SIZE);
	deathsAmt.setFont(font);
}

void HUD::update(unsigned int hitPoints, unsigned int kills, unsigned int deaths) {

	sf::Vector2f size;

	// Sets size of the red rectagle, which covers up green rectagle
	size.x = cfg::HEALTH_BAR_WIDTH - (cfg::HEALTH_BAR_WIDTH * hitPoints) / cfg::MAX_HP;
	size.y = foreGround.getSize().y;
	foreGround.setSize(size);

	killsAmt.setString("Kills: " + std::to_string(kills));
	deathsAmt.setString("Deaths: " + std::to_string(deaths));
}

void HUD::setPos(sf::Vector2f pos) {
	// Receives possition of the top rith corner of the viev
	pos += sf::Vector2f(-cfg::HEALTH_BAR_WIDTH -25, 25);

	backGround.setPosition(pos);
	foreGround.setPosition(pos);

	pos += sf::Vector2f(32, 30);
	killsAmt.setPosition(pos);

	pos += sf::Vector2f(0, 30);
	deathsAmt.setPosition(pos);
}

void HUD::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	// apply the transform
	states.transform *= getTransform();

	// draw shape
	target.draw(backGround, states);
	target.draw(foreGround, states);
	target.draw(killsAmt, states);
	target.draw(deathsAmt, states);
}