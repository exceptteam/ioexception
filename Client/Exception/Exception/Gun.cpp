#include "Gun.h"
#include "Bullet.h"
#include "LaserShot.h"
#include "AuxFunctions.h"

#include <SFML/Graphics.hpp>

Gun::Gun() :
	lasers_amt(cfg::INIT_LASERS),
	bullets_amt(cfg::INIT_BULLETS)
{
	bulletSpeed = 480.0f;
	range = 2.0f;
	cooldown = 0.0f;
}

std::unique_ptr<ForegroundObject> Gun::fire(sf::Vector2f playerPos, sf::Vector2f mousePos, Map* mapPtr, bool primaryAttack, std::string shooterID) {
	/* No secondary attacks
	/* And no ammunition limit

	/*
	if (primaryAttack) {
		if (lasers_amt) {
		*/
			setCooldown();
			std::unique_ptr<ForegroundObject> ptr(new LaserShot(playerPos, mousePos, mapPtr));
	//		lasers_amt--;
			return ptr;
			/*
		}
		else
			return nullptr;
	}

	else {
		if (bullets_amt) {
			std::unique_ptr<ForegroundObject> ptr(new Bullet(playerPos, bulletSpeed*normalize(mousePos), range, mapPtr, shooterID));
			bullets_amt--;
			return ptr;
		}
		else
			return nullptr;
	}
	*/
}

unsigned int Gun::getLasersAmt() {
	return lasers_amt;
}

unsigned int Gun::getBulletsAmt() {
	return bullets_amt;
}

void Gun::updateCooldown(float dt) {
	if (cooldown > 0) {
		cooldown -= dt;
		if (cooldown < 0) {
			cooldown = 0;
		}
	}
}

void Gun::setCooldown() {
	cooldown = cfg::LASER_COOLDOWN_IN_SEC;
	;
}