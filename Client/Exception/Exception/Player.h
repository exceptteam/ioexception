#ifndef _Player_h
#define _Player_h

#include <SFML/Graphics.hpp>

#include "Map.h"
#include "Gun.h"
#include "ForegroundObject.h"
#include "Config.h"
#include "Character.h"

class Player : public Character {
public:
	Player(sf::Vector2f, Map*, bool);
	std::unique_ptr<ForegroundObject> fireGun(sf::Vector2f mousePos, bool primaryAttack);
	void update(float dt);

	unsigned int getLasersAmt();
	unsigned int getBulletsAmt();
	unsigned int getHealthAmt();

	void setHealthAmt(unsigned int);
	
	std::string getDirectionString();
	void updateDirection(); // Must be called only once for one game loop cycle!
	bool getDirectionChanged();

	void setModelPosition(sf::Vector2f);

	void updateGunCooldown(float time) { gun.updateCooldown(time); };
	float getGunCooldown() { return gun.getCooldown(); };

private:
	cfg::CharacterDirection direction;
	bool directionChanged;
	Gun gun;
	unsigned int health_amt;
};

#endif