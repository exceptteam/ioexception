#include <iostream>
#include <string>
#include <memory>

#include "Network.h"
#include "Game.h"
#include "Config.h"

int main()
{
	// Read player name
	std::wstring playerName(L"");
	while (playerName.empty()) {
		std::cout << "Pass your nickname:\n";
		std::getline(std::wcin, playerName);
	}

	// Read server IP
	std::string serverIP;
	bool connected = false;
	std::shared_ptr<Game> game;

	while (!connected) {
		std::cout << "Pass the server ip (leave blank or localhost):\n";
		std::getline(std::cin, serverIP);

		if (serverIP == "")
			serverIP = cfg::SERVER_ADRESS;

		try {
			game = std::make_shared<Game> (serverIP, playerName);
			connected = true;
		}
		catch (ServerConnectionError err) {
			std::cerr << err.what() << " Given IP: " << serverIP << "\n";
			std::cout << "Retry? Leave blank to continue or any key to terminate:\n";
			std::string command;
			std::getline(std::cin, command);
			if (!command.empty())
				return 0;
		}
	}

	game->run();
	return 0;
}