#ifndef _CollisionFunctions_h
#define _CollisionFunctions_h

#include <SFML/Graphics.hpp>

#include "Map.h"
#include "Config.h"


/*
Returns a dot product of two vectors.
*/
float dotProduct(sf::Vector2f, sf::Vector2f);

/*
Returns true if given section collides with given circle.
Details what vector d and f mean can be find there:
http://stackoverflow.com/questions/1073336/circle-line-collision-detection
*/
bool collisionSectionCircle(sf::Vector2f d, sf::Vector2f f, float radius);

/*
Returns true if rectangle defined by its top left vertex and length of
horizontal and vertical side collides with given circle.
*/
bool collisionRectangleCricle(sf::Vector2f squareTopLeft,
	float horizontalSide, float verticalSide, sf::Vector2f circleCenter, float radius);

#endif