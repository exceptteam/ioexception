#ifndef _Message_h
#define _Message_h

#include <string>

class Message
{
public:
	Message();
	Message(std::wstring, std::wstring);
	~Message();

	std::wstring getMessage();
	std::wstring getAuthor();
	void setMessage(std::wstring);

private:
	std::wstring author;
	std::wstring messageText;
};

#endif