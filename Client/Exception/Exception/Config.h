#ifndef _Config_h
#define _Config_h

namespace cfg {
	enum CharacterDirection {
		None = 0,
		North = 1,
		NE = 2,
		East = 3,
		SE = 4,
		South = 5,
		SW = 6,
		West = 7,
		NW = 8
	};

	static const std::string DIRECTION_STRING[] = {
		"None",
		"North",
		"NE",
		"East",
		"SE",
		"South",
		"SW",
		"West",
		"NW"
	};

	//auxiliary
	static const float PI = 3.14159265f;
	static const float SQR_ROOT_2 = 1.41421356f;

	//window
	static const unsigned int WINDOW_WIDTH = 1366;
	static const unsigned int WINDOW_HEIGHT = 768;
	static const std::string WINDOW_TITLE = "Exception";
	

	//network
	static const std::string SERVER_ADRESS = "127.0.0.1";
	static const unsigned int SERVER_PORT = 11000;
	static const unsigned int MAX_SERVER_MSG_SIZE = 50 * 1024; // in bytes

	//player
	static const unsigned int DIRECTIONS_AMT = 4; // WSAD
	static const float PLAYER_SPEED = 400.f; // pixels per second
	static const float PLAYER_SPEED_DIAGONALLY = PLAYER_SPEED / SQR_ROOT_2;
	static const float PLAYER_RADIUS = 25.f;

	//map
	static const unsigned int TILE_SIZE = 32;
	static const unsigned int TILE_BREAK_SIZE = 2;
	static const unsigned int TILE_FULL_SIZE = TILE_SIZE + TILE_BREAK_SIZE;
	static const unsigned int MAP_WIDTH = 100;
	static const unsigned int MAP_HEIGHT = 100;

	//texture paths
	static const std::string PLAYER_TEXTURE_PATH = "../Textures/player.png";
	static const std::string TILE_TEXTURE_PATH = "../Textures/town-tiles.png";

	//fonts
	static const std::string FONT_PATH = "../Resources/Xolonium-Regular.otf";

	//player
	static const unsigned int MAX_HP = 10;
	static const unsigned int INIT_LASERS = 3;
	static const unsigned int INIT_BULLETS = 20;

	//hud
	static const float HEALTH_BAR_WIDTH = 150.f;
	static const float HEALTH_BAR_HEIGTH = 15.f;
	static const float HEALTH_BAR_OUTLINE = 5.f;
	static const unsigned int CHARACTER_SIZE = 15;


	// laser shots
	static const float LASER_TIME_ON_SCREEN = .6f;
	static const float LASER_COOLDOWN_IN_SEC = 1.f;
}

#endif