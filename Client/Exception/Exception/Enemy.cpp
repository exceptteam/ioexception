#include "Enemy.h"

Enemy::Enemy(std::string id_, std::string name, Map* mPtr, sf::Vector2f initPos, bool dead) : 
	Character(initPos, mPtr, dead), name(name) {
	id = id_;
	shape.setColor(sf::Color::Red);
	shape.scale(1.2f, 1.2f);
	updated = true;
}

std::string Enemy::getName() {
	return name;
}

std::string Enemy::getID() {
	return id;
}

void Enemy::update(float dt) {
	if (!getDead())
		move(dt);
}