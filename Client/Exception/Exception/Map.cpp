#include "Map.h"

Map::Map() {}

std::unique_ptr<Tile> Map::collisionSwitch(TileType tileType,
	TileDirection tileDirection, sf::Vector2f topleftCorner) {

	std::unique_ptr<Tile> result;

	switch (tileType)
	{
	case TileType::Wall:
		result = std::unique_ptr<Tile>(new Wall(tileDirection, topleftCorner));
		break;
	case TileType::WideWall:
		result = std::unique_ptr<Tile>(new WideWall(tileDirection, topleftCorner));
		break;

	case TileType::WallCorner:
		result = std::unique_ptr<Tile>(new WallCorner(tileDirection, topleftCorner));
		break;

	case TileType::WideWallCorner:
		result = std::unique_ptr<Tile>(new WideWallCorner(tileDirection, topleftCorner));
		break;

	case TileType::SmallCorner:
		result = std::unique_ptr<Tile>(new SmallCorner(tileDirection, topleftCorner));
		break;

	default:
		result = std::unique_ptr<Tile>(new Tile(tileDirection, topleftCorner));
		break;
	}
	return std::move(result);
}

unsigned int Map::texCoordsSwitch(TileType tileType, TileDirection tileDirection) {
	switch (tileType)
	{
	case TileType::Door:
	case TileType::RoomFloor:
		return 1;

	case TileType::Wall:
		switch (tileDirection)
		{
		case TileDirection::Up:
			return 59;
		case TileDirection::Right:
			return 68;
		case TileDirection::Down:
			return 70;
		case TileDirection::Left:
			return 30;
		}

	case TileType::WideWall:
		switch (tileDirection)
		{
		case TileDirection::Up:
		case TileDirection::Down:
			return 41;
		case TileDirection::Right:
		case TileDirection::Left:
			return 41;
		}

	case TileType::WallCorner:
		switch (tileDirection)
		{
		case TileDirection::Up:
			return 64;
		case TileDirection::Right:
			return 65;
		case TileDirection::Down:
			return 67;
		case TileDirection::Left:
			return 66;
		}

	case TileType::WideWallCorner:
		switch (tileDirection)
		{
		case TileDirection::Up:
			return 41;
		case TileDirection::Right:
			return 41;
		case TileDirection::Down:
			return 41;
		case TileDirection::Left:
			return 41;
		}

	// Small corner has always the same direction
	case TileType::SmallCorner:
		return 63;

	// Out tileset is so poor we use water as roads
	case TileType::Road:
	case TileType::RoadEdge:
	case TileType::RoadDelimiter:
		return 72;

	//case TileType::Grass:
	default:
		switch (tileDirection)
		{
		case TileDirection::Up:
			return 34;
		case TileDirection::Right:
			return 34;
		case TileDirection::Down:
			return 34;
		case TileDirection::Left:
			return 34;
		}
	}
}

bool Map::load(const std::string& tilesetData, mapArray& mapData) {
	// Set the map
	map = mapData;
	unsigned int width = map.size();
	unsigned int height = width > 0 ? map[0].size() : 0;

	// Load the tileset texture
	if (!tileset.loadFromFile(tilesetData))
		return false;

	// Resize the vertex array to fit the map size
	vertices.setPrimitiveType(sf::Quads);
	vertices.resize(width * height * 4);

	// Populate the vertex array with one quad per tile
	for (unsigned int i = 0; i < width; ++i) {
		for (unsigned int j = 0; j < height; ++j) {
			int tileCompressed = map[i][j];

			TileType tileType = static_cast<TileType>(tileCompressed >> 2);
			TileDirection tileDirection = static_cast<TileDirection>(tileCompressed % 4);

			sf::Vector2f topleftCorner(i * cfg::TILE_SIZE, j * cfg::TILE_SIZE);

			// Collision detection switch
			tiles[i][j] = collisionSwitch(tileType, tileDirection, topleftCorner);

			// Texture coords switch
			unsigned int tileTexCoords = texCoordsSwitch(tileType, tileDirection);

			// find its position in the tileset texture
			int tu = tileTexCoords % 8;
			int tv = tileTexCoords / 8;

			// get a pointer to the current tile's quad
			sf::Vertex* quad = &vertices[(i + j * width) * 4];

			// define its 4 corners
			quad[0].position = sf::Vector2f(i * cfg::TILE_SIZE, j * cfg::TILE_SIZE);
			quad[1].position = sf::Vector2f((i + 1) * cfg::TILE_SIZE, j * cfg::TILE_SIZE);
			quad[2].position = sf::Vector2f((i + 1) * cfg::TILE_SIZE, (j + 1) * cfg::TILE_SIZE);
			quad[3].position = sf::Vector2f(i * cfg::TILE_SIZE, (j + 1) * cfg::TILE_SIZE);

			// define its 4 texture coordinates
			quad[0].texCoords = sf::Vector2f(tu * cfg::TILE_FULL_SIZE, tv * cfg::TILE_FULL_SIZE);
			quad[1].texCoords = sf::Vector2f((tu + 1) * cfg::TILE_FULL_SIZE - cfg::TILE_BREAK_SIZE, tv * cfg::TILE_FULL_SIZE);
			quad[2].texCoords = sf::Vector2f((tu + 1) * cfg::TILE_FULL_SIZE - cfg::TILE_BREAK_SIZE, (tv + 1) * cfg::TILE_FULL_SIZE - cfg::TILE_BREAK_SIZE);
			quad[3].texCoords = sf::Vector2f(tu * cfg::TILE_FULL_SIZE, (tv + 1) * cfg::TILE_FULL_SIZE - cfg::TILE_BREAK_SIZE);
		}
	}
	return true;
}

void Map::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	// apply the transform
	states.transform *= getTransform();

	// apply the tileset texture
	states.texture = &tileset;

	// draw the vertex array
	target.draw(vertices, states);
}

bool Map::isTileColiding(unsigned int i, unsigned int j, sf::Vector2f playerPos, float radius) {
	if (i >= cfg::MAP_HEIGHT || j >= cfg::MAP_WIDTH)
		return false;
	return tiles[i][j]->isColiding(playerPos, radius);
}