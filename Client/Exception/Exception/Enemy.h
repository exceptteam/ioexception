#ifndef _Enemy_h
#define _Enemy_h

#include <SFML/Graphics.hpp>
#include "Config.h"
#include "Map.h"
#include "Character.h"

class Enemy : public Character {
public:
	Enemy(std::string id, std::string name, Map* mapPtr, sf::Vector2f initPos, bool dead);
	std::string getName();
	std::string getID();
	void update(float dt);
	bool isUpdated() { return updated; };
	void markAsUpdated(bool mark) { updated = mark; };

private:
	std::string name;
	bool updated;
};

#endif