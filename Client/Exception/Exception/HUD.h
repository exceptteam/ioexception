#include <SFML/Graphics.hpp>

#include "Config.h"

class HUD : public sf::Drawable, public sf::Transformable
{
public:
	HUD();
	void draw(sf::RenderTarget&, sf::RenderStates) const;

	void update(unsigned int hitPoints, unsigned int kills, unsigned int deaths);
	void setPos(sf::Vector2f);

private:
	sf::Font font;
	sf::RectangleShape backGround, foreGround;
	sf::Text killsAmt, deathsAmt;
};
