#ifndef _Network_h
#define _Network_h

#include <SFML/Network.hpp>


class ServerConnectionError : public std::exception
{
public:
	virtual const char* what() const throw()
	{
		return "Unable to connect to given IP";
	}
};


class Network {
public:
	Network(sf::IpAddress ipAddress, unsigned int port);
	sf::TcpSocket::Status receive(void *data, std::size_t size, std::size_t &received);
	sf::TcpSocket::Status send(void *data, std::size_t size);

private:
	sf::TcpSocket socket;
};

#endif