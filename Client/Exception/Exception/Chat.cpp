#include "Chat.h"


Chat::Chat()
{
	if (!font.loadFromFile(cfg::FONT_PATH)) {
		// error..
	}

	currentMessage.setFont(font);
	currentMessage.setCharacterSize(CHARACTER_SIZE);
	currentMessage.setColor(sf::Color::Yellow);
}


Chat::~Chat()
{
}


Message Chat::getLastMessage() {
	return gameLog.back();
}

void Chat::addMessage(Message msg) {
	gameLog.push_back(msg);
	addVisibleMessage(msg);
}

void Chat::addVisibleMessage(Message msg) {
	std::wstring text = msg.getAuthor();
	text += L": ";
	text += msg.getMessage();

	visibleMessages.push_front(sf::Text(text, font, CHARACTER_SIZE));
	timeLeft.push_front(TIME_ON_SCREEN);

	// Alwayas keep no more than MAX_ON_SCREEN messages on visibleMessages list.
	if (visibleMessages.size() > MAX_ON_SCREEN) {
		visibleMessages.pop_back();
		timeLeft.pop_back();
	}
}

void Chat::update(float dt) {
	auto msgIt = visibleMessages.begin();
	auto timeIt = timeLeft.begin();

	while (msgIt != visibleMessages.end()) {
		(*timeIt) -= dt;

		// Remove from both lists.
		if ((*timeIt) < 0) {
			visibleMessages.erase(msgIt++);
			timeLeft.erase(timeIt++);
		}
		else {
			msgIt++;
			timeIt++;
		}
	}
}

void Chat::removeAllMessages() {
	gameLog.clear();
}

void Chat::removeMessages(int n) {
	gameLog.erase(gameLog.begin(), gameLog.begin() + n);
}

const std::list<sf::Text> & Chat::getVisibleMessages(sf::Vector2f position) {
	for (auto it = visibleMessages.rbegin(); it != visibleMessages.rend(); ++it) {
		(*it).setPosition(position);
		position.y += MESSAGE_DISTANCE;
	}
	return visibleMessages;
}

sf::Text Chat::getVisibleMessage(sf::Vector2f pos, std::wstring text) {
	currentMessage.setPosition(pos);
	currentMessage.setString(L"Chat: " + text);

	return currentMessage;
}