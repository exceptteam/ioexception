#ifndef _Character_h
#define _Character_h

#include <SFML/Graphics.hpp>
#include <vector>
#include "Config.h"
#include "Map.h"

class Character : public sf::Drawable, public sf::Transformable {
public:
	enum Direction
	{
		UP = 0,
		DOWN = 1,
		LEFT = 2,
		RIGHT = 3
	};

	Character(sf::Vector2f initPos, Map* mPtr, bool dead);
	void setAngle(float);
	void setDirection(Direction, bool);
	void setDirection(cfg::CharacterDirection);
	bool tryToSetCharacterPos(sf::Vector2f & pos);
	void setPos(sf::Vector2f pos);
	void setDead(bool d);
	sf::Vector2f getPosition();
	float getAngle();
	std::string getID() { return id; };
	bool getDead() { return dead; };
	virtual void update(float dt) = 0;

protected:
	enum class ResultantVer
	{
		NORTH = -1,
		NONE = 0,
		SOUTH = 1
	};

	enum class ResultantHor
	{
		WEST = -1,
		NONE = 0,
		EAST = 1
	};

	bool detectCollision(sf::Vector2f futurePos);
	sf::Vector2f move(float dt);
	void getResultants(ResultantVer & resultantVer, ResultantHor & resultantHor);

	std::vector<bool> directions;
	float angle;
	sf::Sprite shape;
	sf::Texture playerTexture;
	Map* mapPtr;
	sf::CircleShape circ;
	std::string id;
	bool dead;

	virtual void draw(sf::RenderTarget&, sf::RenderStates) const;
};

#endif