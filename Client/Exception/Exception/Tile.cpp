#include"Tile.h"
#include "AuxFunctions.h"
#include "CollisionFunctions.h"

Tile::Tile(TileDirection tileDirection, sf::Vector2f topLeftCorner) :
	tileDirection(tileDirection),
	topLeftCorner(topLeftCorner)
{}

WideWall::WideWall( TileDirection tileDirection, sf::Vector2f topLeftCorner) :
	Tile(tileDirection, topLeftCorner)
{
	rects[0] = { topLeftCorner, SIDE, SIDE }; //Up
	rects[1] = { topLeftCorner, SIDE, SIDE }; //Right
	rects[2] = { topLeftCorner, SIDE, SIDE }; //Down
	rects[3] = { topLeftCorner, SIDE, SIDE }; //Left
}

Wall::Wall(TileDirection tileDirection, sf::Vector2f topLeftCorner) :
Tile(tileDirection, topLeftCorner)
{
	rects[0] = { sf::Vector2f(topLeftCorner.x, topLeftCorner.y), LONG_SIDE, SHORT_SIDE }; //Up
	rects[1] = { sf::Vector2f(topLeftCorner.x + SHORT_SIDE, topLeftCorner.y), SHORT_SIDE, LONG_SIDE }; //Right	
	rects[2] = { sf::Vector2f(topLeftCorner.x, topLeftCorner.y + SHORT_SIDE), LONG_SIDE, SHORT_SIDE }; //Down
	rects[3] = { sf::Vector2f(topLeftCorner.x, topLeftCorner.y), SHORT_SIDE, LONG_SIDE }; //Left
}

WallCorner::WallCorner(TileDirection tileDirection, sf::Vector2f topLeftCorner) :
	Tile(tileDirection, topLeftCorner)
{
	rects[0][0] = { sf::Vector2f(topLeftCorner.x + SHORT_SIDE, topLeftCorner.y), SHORT_SIDE, LONG_SIDE }; //Up
	rects[0][1] = { topLeftCorner, SHORT_SIDE, SHORT_SIDE }; //Up
	rects[1][0] = { sf::Vector2f(topLeftCorner.x + SHORT_SIDE, topLeftCorner.y), SHORT_SIDE, LONG_SIDE }; //Right
	rects[1][1] = { sf::Vector2f(topLeftCorner.x, topLeftCorner.y + SHORT_SIDE), SHORT_SIDE, SHORT_SIDE }; //Right
	rects[2][0] = { sf::Vector2f(topLeftCorner.x, topLeftCorner.y), SHORT_SIDE, LONG_SIDE }; //Down
	rects[2][1] = { sf::Vector2f(topLeftCorner.x + SHORT_SIDE, topLeftCorner.y + SHORT_SIDE), SHORT_SIDE, SHORT_SIDE }; //Down
	rects[3][0] = { sf::Vector2f(topLeftCorner.x, topLeftCorner.y), SHORT_SIDE, LONG_SIDE }; //Left
	rects[3][1] = { sf::Vector2f(topLeftCorner.x + SHORT_SIDE, topLeftCorner.y), SHORT_SIDE, SHORT_SIDE }; //Left
}

WideWallCorner::WideWallCorner(TileDirection tileDirection, sf::Vector2f topLeftCorner) :
	Tile(tileDirection, topLeftCorner)
{
	rects[0] = { topLeftCorner, SIDE, SIDE }; //Up
	rects[1] = { topLeftCorner, SIDE, SIDE }; //Right
	rects[2] = { topLeftCorner, SIDE, SIDE }; //Down
	rects[3] = { topLeftCorner, SIDE, SIDE }; //Left
}

SmallCorner::SmallCorner(TileDirection tileDirection, sf::Vector2f topLeftCorner) :
	Tile(tileDirection, topLeftCorner)
{
	rects[0] = { sf::Vector2f(topLeftCorner.x + SIDE, topLeftCorner.y + SIDE), SIDE, SIDE }; //Up
	rects[1] = { sf::Vector2f(topLeftCorner.x + SIDE, topLeftCorner.y + SIDE), SIDE, SIDE }; //Right
	rects[2] = { sf::Vector2f(topLeftCorner.x + SIDE, topLeftCorner.y + SIDE), SIDE, SIDE }; //Down
	rects[3] = { sf::Vector2f(topLeftCorner.x + SIDE, topLeftCorner.y + SIDE), SIDE, SIDE }; //Left
}

bool WideWall::isColiding(const sf::Vector2f playerPos, const float radius) {
	return collisionRectangleCricle(rects[tileDirection].topLeftCorner, 
		rects[tileDirection].horizontalSide, rects[tileDirection].verticalSide, playerPos, radius);
}

bool Wall::isColiding(const sf::Vector2f playerPos, const float radius) {
	return collisionRectangleCricle(rects[tileDirection].topLeftCorner,
		rects[tileDirection].horizontalSide, rects[tileDirection].verticalSide, playerPos, radius);
}

bool WideWallCorner::isColiding(const sf::Vector2f playerPos, const float radius) {
	return collisionRectangleCricle(rects[tileDirection].topLeftCorner,
		rects[tileDirection].horizontalSide, rects[tileDirection].verticalSide, playerPos, radius);
}

bool WallCorner::isColiding(const sf::Vector2f playerPos, const float radius) {
	bool collision1 = collisionRectangleCricle(rects[tileDirection][0].topLeftCorner,
		rects[tileDirection][0].horizontalSide, rects[tileDirection][0].verticalSide, playerPos, radius);

	bool collision2 = collisionRectangleCricle(rects[tileDirection][1].topLeftCorner,
		rects[tileDirection][1].horizontalSide, rects[tileDirection][1].verticalSide, playerPos, radius);

	return collision1 || collision2;
}

bool SmallCorner::isColiding(const sf::Vector2f playerPos, const float radius) {
	return collisionRectangleCricle(rects[tileDirection].topLeftCorner,
		rects[tileDirection].horizontalSide, rects[tileDirection].verticalSide, playerPos, radius);
}