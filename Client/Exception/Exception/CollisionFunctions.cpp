#include "CollisionFunctions.h"


float dotProduct(sf::Vector2f a, sf::Vector2f b) {
	return a.x*b.x + a.y*b.y;
}

bool collisionSectionCircle(sf::Vector2f d, sf::Vector2f f, float radius) {
	float a = dotProduct(d, d);
	float b = 2.f * dotProduct(f, d);
	float c = dotProduct(f, f) - radius*radius;

	float discriminant = b*b - 4 * a*c;

	if (discriminant < 0.)
		return false;

	// ray didn't totally miss sphere,
	// so there is a solution to
	// the equation.

	discriminant = sqrt(discriminant);

	// either solution may be on or off the ray so need to test both
	// t1 is always the smaller value, because BOTH discriminant and
	// a are nonnegative.
	float t1 = (-b - discriminant) / (2.f * a);
	float t2 = (-b + discriminant) / (2.f * a);

	//std::cout << "t1: " << t1 << " t2: " << t2 << std::endl;

	// 3x HIT cases:
	//          -o->             --|-->  |            |  --|->
	// Impale(t1 hit,t2 hit), Poke(t1 hit,t2>1), ExitWound(t1<0, t2 hit), 

	// 3x MISS cases:
	//       ->  o                     o ->              | -> |
	// FallShort (t1>1,t2>1), Past (t1<0,t2<0), CompletelyInside(t1<0, t2>1)

	if (t1 >= 0 && t1 <= 1)
	{
		// t1 is the intersection, and it's closer than t2
		// (since t1 uses -b - discriminant)
		// Impale, Poke
		return true;
	}

	// here t1 didn't intersect so we are either started
	// inside the sphere or completely past it
	if (t2 >= 0 && t2 <= 1)
	{
		// ExitWound
		return true;
	}

	// no intn: FallShort, Past, CompletelyInside
	return false;
}

bool collisionRectangleCricle(sf::Vector2f squareTopLeft,
	float horizontalSide, float verticalSide, sf::Vector2f circleCenter, float radius) {

	// algorithm from http://stackoverflow.com/questions/1073336/circle-line-collision-detection

	// squareTopLeft is coords of the top left corner of the considered square
	sf::Vector2f sides[4][2] = {
			{ sf::Vector2f(0, 0), sf::Vector2f(horizontalSide, 0) }, // Top side
			{ sf::Vector2f(horizontalSide, 0), sf::Vector2f(horizontalSide, verticalSide) }, // Right side
			{ sf::Vector2f(0, verticalSide), sf::Vector2f(horizontalSide, verticalSide) }, // Bottom side
			{ sf::Vector2f(0, 0), sf::Vector2f(0, verticalSide) }  // Left side
	};

	// Checking whether circle collides with given side of the square
	for (int i = 0; i < 4; i++) {
		sf::Vector2f L = squareTopLeft + sides[i][0];
		sf::Vector2f E = squareTopLeft + sides[i][1];

		sf::Vector2f  d = L - E;
		sf::Vector2f  f = E - circleCenter;

		if (collisionSectionCircle(d, f, radius))
			return true;

	}
	return false;
}