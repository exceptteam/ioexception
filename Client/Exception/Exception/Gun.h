#ifndef _Gun_h
#define _Gun_h

#include <SFML/Graphics.hpp>

#include "Map.h"
#include "ForegroundObject.h"

class Gun {
public:
	Gun();
	std::unique_ptr<ForegroundObject> fire(sf::Vector2f PlayerPos, sf::Vector2f mousePos, Map* mapPtr, bool primaryAttack, std::string shooterID);

	unsigned int getLasersAmt();
	unsigned int getBulletsAmt();
	void updateCooldown(float);
	void setCooldown();
	float getCooldown() { return cooldown; };

private:
	float bulletSpeed;
	float range;
	unsigned int lasers_amt;
	unsigned int bullets_amt;
	float cooldown;
};

#endif