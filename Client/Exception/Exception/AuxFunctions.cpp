#include "AuxFunctions.h"

sf::Vector2f round(sf::Vector2f vec) {
	return sf::Vector2f(round(vec.x), round(vec.y));
}

sf::Vector2f normalize(sf::Vector2f vec) {
	return vec / sqrt(vec.x*vec.x + vec.y*vec.y);
}

float length(sf::Vector2f vec) {
	return sqrt(vec.x*vec.x + vec.y*vec.y);
}

bool compare_s_ws(std::string str, std::wstring ws) {
	return std::equal(str.begin(), str.end(), ws.begin()) && ws.length() == str.length();
}

cfg::CharacterDirection getDirection(int i) {
	switch (i) {
	case 0:
		return cfg::CharacterDirection::None;

	case 1:
		return cfg::CharacterDirection::North;

	case 2:
		return cfg::CharacterDirection::NE;

	case 3:
		return cfg::CharacterDirection::East;

	case 4:
		return cfg::CharacterDirection::SE;

	case 5:
		return cfg::CharacterDirection::South;

	case 6:
		return cfg::CharacterDirection::SW;

	case 7:
		return cfg::CharacterDirection::West;

	case 8:
		return cfg::CharacterDirection::NW;
	}
}

std::wstring utf8_to_wstring(const std::string& str)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;
	return myconv.from_bytes(str);
}

std::string wstring_to_utf8(const std::wstring& str)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;
	return myconv.to_bytes(str);
}