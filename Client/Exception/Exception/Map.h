#ifndef _Map_h
#define _Map_h

#include <SFML/Graphics.hpp>

#include <vector>
#include <memory>

#include "Tile.h"
#include "Config.h"

class Tile; // TODO why?!
class Character;

typedef std::vector<std::vector<int>> mapArray;
typedef Tile::TileType TileType;
typedef Tile::TileDirection TileDirection;

class Map : public sf::Drawable, sf::Transformable {
public:
	Map();
	void print();
	bool load(const std::string&, mapArray&);
	bool isTileColiding(unsigned int i, unsigned int j, sf::Vector2f playerPos, float radius);
	std::map<std::string, std::shared_ptr<Character>> characters;

private:
	std::unique_ptr<Tile> collisionSwitch(TileType tileType, 
		TileDirection tileDirection, sf::Vector2f topleftCorner);
	unsigned int texCoordsSwitch(TileType tileType, TileDirection tileDirection);

	virtual void draw(sf::RenderTarget&, sf::RenderStates) const;

	std::unique_ptr<Tile> tiles[cfg::MAP_WIDTH][cfg::MAP_HEIGHT];

	mapArray map;
	sf::VertexArray vertices;
	sf::Texture tileset;
};

#endif