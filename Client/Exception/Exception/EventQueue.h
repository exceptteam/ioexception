#ifndef _EventQueue_h
#define _EventQueue_h

#include <set>

#include "Event.h"

class EventQueue {
public:
	EventQueue();
	Event getNextEvent();
	void addEvent(Event);
	unsigned int getSize() { return mQueue.size(); };
	sf::Time getFirstTime();

	struct eventCompare {
		bool operator()(const Event& a, const Event& b) {
			return a.getTimestamp() < b.getTimestamp();
		}
	};


private:
	std::multiset<Event,eventCompare> mQueue;
};

#endif