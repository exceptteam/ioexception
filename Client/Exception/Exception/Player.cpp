#include "Player.h"
#include "AuxFunctions.h"
#include <iostream>

Player::Player(sf::Vector2f initPos, Map* mPtr, bool dead) : 
	Character(initPos, mPtr, dead),
	direction(cfg::CharacterDirection::None),
	directionChanged(false),
	health_amt(cfg::MAX_HP)
{
	circ.setFillColor(sf::Color::Magenta);
	id = "player";
}

void Player::update(float dt) {
	//std::cerr << directions[0] << directions[1] << directions[2] << directions[3] << "\n";
	if (!getDead())
		move(dt);
}

std::unique_ptr<ForegroundObject> Player::fireGun(sf::Vector2f mousePos, bool primaryAttack) {
	if (!getDead())
		return gun.fire(getPosition(), mousePos, mapPtr, primaryAttack, id);
	else
		return nullptr;
}

unsigned int Player::getLasersAmt() {
	return gun.getLasersAmt();
}

unsigned int Player::getBulletsAmt() {
	return gun.getBulletsAmt();
}

unsigned int Player::getHealthAmt(){
	return health_amt;
}

void Player::setHealthAmt(unsigned int health_amt) {
	this->health_amt = health_amt;
}

std::string Player::getDirectionString() {
	return cfg::DIRECTION_STRING[static_cast<int>(direction)];
}

void Player::updateDirection(){
	ResultantVer resultantVer;
	ResultantHor resultantHor;
	getResultants(resultantVer, resultantHor);

	cfg::CharacterDirection previousDireaction = direction;

	if (resultantVer == ResultantVer::NONE && resultantHor == ResultantHor::NONE)
		direction = cfg::CharacterDirection::  None;

	else if (resultantVer == ResultantVer::NORTH && resultantHor == ResultantHor::NONE)
		direction = cfg::CharacterDirection::North;

	else if (resultantVer == ResultantVer::NORTH && resultantHor == ResultantHor::EAST)
		direction = cfg::CharacterDirection::NE;

	else if (resultantVer == ResultantVer::NONE && resultantHor == ResultantHor::EAST)
		direction = cfg::CharacterDirection::East;

	else if (resultantVer == ResultantVer::SOUTH && resultantHor == ResultantHor::EAST)
		direction = cfg::CharacterDirection::SE;

	else if (resultantVer == ResultantVer::SOUTH && resultantHor == ResultantHor::NONE)
		direction = cfg::CharacterDirection::South;

	else if (resultantVer == ResultantVer::SOUTH && resultantHor == ResultantHor::WEST)
		direction = cfg::CharacterDirection::SW;

	else if (resultantVer == ResultantVer::NONE && resultantHor == ResultantHor::WEST)
		direction = cfg::CharacterDirection::West;

	else
		direction = cfg::CharacterDirection::NW;

	directionChanged = (previousDireaction != direction);
}

bool Player::getDirectionChanged() {
	return directionChanged;
}

void Player::setModelPosition(sf::Vector2f pos) {
	shape.setPosition(pos);
}