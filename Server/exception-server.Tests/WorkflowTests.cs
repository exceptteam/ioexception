﻿using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using exception_server.Communication;
using exception_server.Config;
using exception_server.Model.Gameplay;
using exception_server.Model.Maps;
using exception_server.Model.Units;
using Newtonsoft.Json;
using NUnit.Framework;

namespace exception_server.Tests
{
    [TestFixture]
    public class WorkflowTests
    {
        protected Game Game { get; set; }
        protected Socket ClientSocket { get; set; }
        protected TcpHandler ClientTcpHandler { get; set; }

        protected NetworkingService NetworkingService { get; set; }

        [SetUp]
        public void StartServer()
        {
            MapConfig.MapHeight = 150;
            MapConfig.MapWidth = 150;
            Game = new Game();
            Game.Start();
            ClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            ClientSocket.Connect(new IPEndPoint(IPAddress.Parse("127.0.0.1"), GameConfig.ServerPort));
            ClientSocket.Blocking = true;

            ClientTcpHandler = new TcpHandler(ClientSocket);
        }

        [TearDown]
        public void Cleanup()
        {
            ClientTcpHandler.Disconnect();
            Game.StopGame();
        }

        [Test]
        public void StartAndStopServer_NoExceptions()
        {
            Assert.True(true);
        }

        [Test]
        public void SendTrash_DisconnectedByTimeout()
        {
            //arrange
            
            //act
            ClientTcpHandler.Send("fghdfghdfgh");
           
            var responseStr = ClientTcpHandler.Recieve();

            //assert
            //no answer
            Assert.IsNullOrEmpty(responseStr);
        }

        [Test]
        public void ClientMovesSountEast_Successfull()
        {
            //arrange
            var mapGenerator = new MapGenerator();
            Game.Map = mapGenerator.Map;


            ClientTcpHandler.Send("{'Command' : 'JoinGame',	'Player' : {'Name' : 'TestPlayer'}}");
            
            //read map
            ClientTcpHandler.Recieve();
            
            var secondResponse = ClientTcpHandler.Recieve();

            JsonConvert.DeserializeObject<ServerResponse>(secondResponse);

            const int START_X = 3;
            Game.Clients[0].Player.X = START_X;
            const int START_Y = 3;
            Game.Clients[0].Player.Y = START_Y;

            //act
            ClientTcpHandler.Send("{'Command' : 'MoveStart', 'Direction' : 'SE'}");

            string strResponse = string.Empty;

            for (int i = 0; i <= 100; i++)
            {
                strResponse = string.Empty;
                while (string.IsNullOrEmpty(strResponse))
                {
                    strResponse = ClientTcpHandler.Recieve();
                }
            }

            var responseNr100 = JsonConvert.DeserializeObject<ServerResponse>(strResponse);

            float playerSpeed = Game.Clients[0].Player.GetSpeed();
            //assert
            const float DIAG_SPEED = 1 / 1.41421356f;
            var finishX = START_X + 100 * playerSpeed * DIAG_SPEED;
            var finishY = START_Y + 100 * playerSpeed * DIAG_SPEED;
            Assert.GreaterOrEqual(responseNr100.Player.X, finishX / 1.01);
            Assert.LessOrEqual(responseNr100.Player.X, finishX * 1.01);
            Assert.GreaterOrEqual(responseNr100.Player.Y, finishY / 1.01);
            Assert.LessOrEqual(responseNr100.Player.Y, finishY * 1.01);
        }

        [Test]
        public void TwoClients_CommunicateViaChat_Succesfully()
        {
            //arrange
            var clientSocket2 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            clientSocket2.Connect(new IPEndPoint(IPAddress.Parse("127.0.0.1"), GameConfig.ServerPort));
            clientSocket2.Blocking = true;

            var secondClient = new TcpHandler(clientSocket2); 

            ClientTcpHandler.Send("{'Command' : 'JoinGame',	'Player' : {'Name' : 'TestPlayer1'}}");
            secondClient.Send("{'Command' : 'JoinGame',	'Player' : {'Name' : 'TestPlayer2'}}");

            //read map
            for (int i = 0; i < 5; i++)
            {
                ClientTcpHandler.Recieve();
                string msg = secondClient.Recieve();
            }

           //act
            const string firstClientMessage = "Hi, I am TestPlayer1";
            ClientTcpHandler.Send(string.Format("{{'Command' : '{0}', 'Message' : '{1}'}}", CommandType.ChatMessage, firstClientMessage));

            const string secondClientMessage = "Hi, I am TestPlayer2";
            secondClient.Send(string.Format("{{'Command' : '{0}', 'Message' : '{1}'}}", CommandType.ChatMessage, secondClientMessage));


            var response1 = new ServerResponse();
            while (response1.ChatMessages == null)
                response1 = JsonConvert.DeserializeObject<ServerResponse>( ClientTcpHandler.Recieve());

            string str;
            var response2 = new ServerResponse();
            while (response2.ChatMessages == null)
            {
                str = secondClient.Recieve();
                response2 = JsonConvert.DeserializeObject<ServerResponse>(str);
            }

            //assert
            Assert.AreEqual(response1.ChatMessages.Where(m => m.SenderName == "TestPlayer2").Select(m => m.Message).FirstOrDefault(), secondClientMessage);
            Assert.AreEqual(response2.ChatMessages.Where(m => m.SenderName == "TestPlayer1").Select(m => m.Message).FirstOrDefault(), firstClientMessage);
        }

        [Test]
        public void ClientMovesRight_StumbleOnWall()
        {
            //arrange
            var mapGenerator = new MapGenerator();
            mapGenerator.GenerateSquareBuilding(0, 0, 6, 6);
            Game.Map = mapGenerator.Map;
          

            ClientTcpHandler.Send("{'Command' : 'JoinGame',	'Player' : {'Name' : 'TestPlayer'}}");

            //read map
            for (int i = 0; i < 5; i++)
                ClientTcpHandler.Recieve();

            var player = Game.ActiveClients[0].Player;
            player.X = 3;
            player.Y = 3;

            //act
            ClientTcpHandler.Send("{'Command' : 'MoveStart', 'Direction' : 'East'}");

            string strResponse = string.Empty;

            for (int i = 0; i <= 100; i++)
            {
                strResponse = string.Empty;
                while (string.IsNullOrEmpty(strResponse))
                {
                    strResponse = ClientTcpHandler.Recieve();
                }
            }

            var responseNr100 = JsonConvert.DeserializeObject<ServerResponse>(strResponse);
           
            //assert
            var finishX = 5 - player.GetUnitRadius();
            Assert.GreaterOrEqual(responseNr100.Player.X, finishX - 0.2f);
            Assert.LessOrEqual(responseNr100.Player.X, finishX * 1.01);
        }

        [Test]
        public void ClientJoinsGame_Successfull()
        {
            //arrange
            //act
            ClientTcpHandler.Send("{'Command' : 'JoinGame',	'Player' : {'Name' : 'TestPlayer'}}");

            var map = ClientTcpHandler.Recieve();

            var firstRequest = ClientTcpHandler.Recieve();
            
            var sw = new Stopwatch();
            sw.Start();
            var secondResponse = ClientTcpHandler.Recieve();
            sw.Stop();

            var response = JsonConvert.DeserializeObject<ServerResponse>(secondResponse);

            //assert
            Assert.Less(sw.ElapsedMilliseconds, 50);
            Assert.IsNotNull(firstRequest);
            Assert.IsNotNull(response);
            Assert.AreEqual(map.Length, MapConfig.MapHeight * MapConfig.MapWidth);
            Assert.IsNotNull(response.Player);
            Assert.AreEqual("TestPlayer", response.Player.Name);
        }
    }
}
