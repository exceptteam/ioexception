﻿using System;
using System.Collections.Generic;
using System.Text;
using exception_server.Communication;
using exception_server.Config;
using NUnit.Framework;
using exception_server.Model.Maps;

namespace exception_server.Tests
{
    [TestFixture]
    public class MapTests
    {
        private MapGenerator MapGenerator { get; set; }
        private Serializer Serializer { get; set; }

        private Map Map { get; set; }

        [SetUp]
        public void Init()
        {
            MapConfig.MapHeight = 12;
            MapConfig.MapWidth = 12;
            MapGenerator = new MapGenerator();
            Map = MapGenerator.Map;
            Serializer = new Serializer();
        }

        [Test]
        public void GenerateVerticalRoad()
        {
            //arrange
            String[] map =
            {
                "    [░░░░░░░┊┊░░░░░░░]  ",
                "    [░░░░░░░┊┊░░░░░░░]  ",
                "    [░░░░░░░┊┊░░░░░░░]  ",
                "    [░░░░░░░┊┊░░░░░░░]  ",
                "    [░░░░░░░┊┊░░░░░░░]  ",
                "    [░░░░░░░┊┊░░░░░░░]  ",
                "    [░░░░░░░┊┊░░░░░░░]  ",
                "    [░░░░░░░┊┊░░░░░░░]  ",
                "    [░░░░░░░┊┊░░░░░░░]  ",
                "    [░░░░░░░┊┊░░░░░░░]  ",
                "                        ",
                "                        "
            };
            var sBuilder = new StringBuilder();
            foreach (var str in map)
            {
                sBuilder.AppendLine(str);
            }

            //act
            MapGenerator.GenerateVerticalRoad(2, 0, 9, 10);

            //print log
            Serializer.PrintMapToLog(Map);

            var generatedMap = Serializer.SerializeMapForLog(Map);

            //assert
            Assert.AreEqual(sBuilder.ToString(), generatedMap);
        }

        [Test]
        public void GenerateHorizontalRoad()
        {
            //arrange
            String[] map =
            {
                "    ────────────────────",
                "    ░░░░░░░░░░░░░░░░░░░░",
                "    ░░░░░░░░░░░░░░░░░░░░",
                "    ░░░░░░░░░░░░░░░░░░░░",
                "    ====================",
                "    ░░░░░░░░░░░░░░░░░░░░",
                "    ░░░░░░░░░░░░░░░░░░░░",
                "    ░░░░░░░░░░░░░░░░░░░░",
                "    ░░░░░░░░░░░░░░░░░░░░",
                "    ────────────────────",
                "                        ",
                "                        "
            };
            var sBuilder = new StringBuilder();
            foreach (var str in map)
            {
                sBuilder.AppendLine(str);
            }

            //act
            MapGenerator.GenerateHorizontalRoad(2, 0, 10, 10);

            //print log
            Serializer.PrintMapToLog(Map);

            var generatedMap = Serializer.SerializeMapForLog(Map);

            //assert
            Assert.AreEqual(sBuilder.ToString(), generatedMap);
        }

        [Test]
        public void GenerateRoomHolders()
        {
            var roomHolders = MapGenerator.GenerateRoomHolders(200, 100, 30, 42);
           
            //30*42 = 1260 tiles. Average room holder is 6x9 tiles, so, there should be 18-30 holders
            Assert.GreaterOrEqual(roomHolders.Count, 18);
            Assert.LessOrEqual(roomHolders.Count, 30);
        }

        [Test]
        public void GenerateComplexBuilding()
        {
            //arrange
            MapConfig.MapHeight = 41;
            MapConfig.MapWidth = 41;
            MapGenerator.Random = new Random(2014);
            MapGenerator = new MapGenerator();
            Map = MapGenerator.Map;

            //act
            MapGenerator.GenerateComplexBuilding(1, 1, 38, 38, 50);

            //print log
            Serializer.PrintMapToLog(Map);

            var tilesCount = CountTileTypesFrequency();
            
            Assert.Greater(tilesCount[Tile.TileType.Wall], 60);
            Assert.Greater(tilesCount[Tile.TileType.Door], 15);
            Assert.Greater(tilesCount[Tile.TileType.WideWall], 100);
            Assert.Greater(tilesCount[Tile.TileType.RoomFloor], 500);
        }

        [Test]
        public void GenerateTempMap()
        {
            //arrange
            MapConfig.MapHeight = 100;
            MapConfig.MapWidth = 100;
            
            MapGenerator = new MapGenerator {Random = new Random(2014)};
            Map = MapGenerator.Map;

            //act
            MapGenerator.GenerateTempMap();

            //print log
            Serializer.PrintMapToLog(Map);

            var tilesCount = CountTileTypesFrequency();
            int area = MapConfig.MapHeight*MapConfig.MapWidth;

            Assert.Greater(tilesCount[Tile.TileType.Road], 0.08f * area);
            Assert.Greater(tilesCount[Tile.TileType.RoadEdge], 0.03f * area);
            Assert.AreEqual(tilesCount[Tile.TileType.RoadDelimiter], 98);
            Assert.Greater(tilesCount[Tile.TileType.WideWall], 0.07f * area);
            Assert.Greater(tilesCount[Tile.TileType.Wall], 0.019f * area);
            Assert.Greater(tilesCount[Tile.TileType.Door], 0.005f * area);
            Assert.Greater(tilesCount[Tile.TileType.RoomFloor], 0.18f * area);
        }

        private Dictionary<Tile.TileType, int> CountTileTypesFrequency()
        {
            var tilesCount = new Dictionary<Tile.TileType, int>();
            for (var i = 0; i < MapConfig.MapWidth; i++)
            {
                for (var j = 0; j < MapConfig.MapHeight; j++)
                {
                    var tile = Map[j, i];
                    if (tilesCount.ContainsKey(tile.Type))
                        tilesCount[tile.Type]++;
                    else
                        tilesCount[tile.Type] = 1;
                }
            }
            return tilesCount;
        }


        [Test]
        public void SerializeBuilding()
        {
            //arrange
            string expectedStr = System.IO.File.ReadAllText(@"..\..\serializedBuilding.txt", Encoding.ASCII);

            //act
            MapGenerator.GenerateSquareBuilding(3, 1, 6, 10);

            //print log
            Serializer.PrintMapToLog(Map);
            var serializedMap = Serializer.SerializeMap(Map);

            //assert
            Assert.AreEqual(expectedStr, serializedMap);
        }


        [Test]
        public void GenerateBuilding()
        {
            //arrange
            String[] map =
            {
                "                        ",
                "      ████████████      ",
                "      ██ . . . .██      ",
                "      ██ . . . .██      ",
                "      ██ . . . .██      ",
                "      ██ . . . .██      ",
                "      ██ . . . .██      ",
                "      ██ . . . .██      ",
                "      ██ . . . .██      ",
                "      ██ . . . .██      ",
                "      ████████████      ",
                "                        "
            };
            var sBuilder = new StringBuilder();
            foreach (var str in map)
            {
                sBuilder.AppendLine(str);
            }

            //act
            MapGenerator.GenerateSquareBuilding(3, 1, 6, 10);

            //print log
            Serializer.PrintMapToLog(Map);
            var generatedMap = Serializer.SerializeMapForLog(Map);

            //assert
            Assert.AreEqual(sBuilder.ToString(), generatedMap);
        }
    }
}
