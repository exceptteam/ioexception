﻿using exception_server.Config;
using exception_server.Model.Units;
using NUnit.Framework;

namespace exception_server.Tests
{
    [TestFixture]
    public class UnitsTests
    {

        private UnitGenerator Generator { get; set; }

        [SetUp]
        public void Init()
        {
            Generator = new UnitGenerator();
        }

        [Test]
        public void GenerateMale()
        {
            //arrange
            var player = new Unit();
            var maleParams = UnitConfig.ParamsByType[UnitType.MaleHuman];
            //act
            UnitGenerator.GenerateUnitCharacteristics(player);

            //assert
            Assert.AreEqual(player.Type, UnitType.MaleHuman);
            Assert.IsFalse(string.IsNullOrEmpty(player.Id));
            Assert.IsFalse(string.IsNullOrEmpty(player.Name));
            Assert.AreEqual(maleParams, player.Params);
        }
    }
}
