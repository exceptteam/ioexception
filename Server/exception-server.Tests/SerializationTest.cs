﻿using exception_server.Communication;
using exception_server.Model.Gameplay;
using exception_server.Model.Units;
using NUnit.Framework;

namespace exception_server.Tests
{
    [TestFixture]
    public class SerializationTest
    {
        private Serializer Serializer { get; set; }
        [SetUp]
        public void Init()
        {
            Serializer = new Serializer();
        }

        [Test]
        public void DeserializeMoveCommand()
        {
            //arrange
            string command = @"{                   
                   'Command': 'MoveStart',
                   'TimeStamp' : 43334,
                   'Direction' : 1
                 }";

            //act
            var res = (MoveStartCommand)Serializer.DeserializeRequestCommand(command);

            //assert
            Assert.AreEqual(res.Direction, MoveDirection.North);
            Assert.AreEqual(res.TimeStamp, 43334);
        }

        [Test]
        public void DeserializeJoinGameCommand()
        {
            //arrange
            string command = @"{                   
                   'Command': 'JoinGame',                   
                   'Player' : {
                        'Name' : 'John'
                    }
                 }";

            //act
            var res = (JoinGameCommand)Serializer.DeserializeRequestCommand(command);

            //assert
            Assert.AreEqual(res.Player.Name, "John");
        }

        [Test]
        public void SerializePlayer()
        {
            //arrange
            var player = new Unit {Name = "Test", Type = UnitType.FemaleHuman, X = 200, Y = 100};
            UnitGenerator.GenerateUnitCharacteristics(player);
            var response = new ServerResponse {Player = player};

            string expexted = string.Format("{{\r\n  \"TimeStamp\": 0,\r\n  \"Player\": {{\r\n    \"Id\": \"{0}\",\r\n    \"Kills\": 0,\r\n    \"Deaths\": 0,\r\n    \"X\": 200.0,\r\n    \"Y\": 100.0,\r\n    \"Direction\": 0,\r\n    \"Name\": \"Test\",\r\n    \"CurHp\": 9,\r\n    \"MaxHp\": 9,\r\n    \"Angle\": 0.0\r\n  }}\r\n}}", player.Id);

            //act
            var serialized = Serializer.SerializeResponse(response);
            
            //assert
            Assert.AreEqual(expexted, serialized);
        }
    }
}
