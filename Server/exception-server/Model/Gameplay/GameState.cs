﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using exception_server.Config;
using exception_server.Model;
using exception_server.Model.Units;

namespace exception_server.Model.Gameplay
{
    public class GameHistory
    {
        private readonly int historyLength;
        private readonly GameState[] history;

        public GameHistory()
        {
            historyLength = GameConfig.TicksCountToRemember;
            history = new GameState[historyLength];
        }

        public void SaveGameState(GameState state)
        {
            history[state.Tick%historyLength] = state;
        }

        public GameState GetGameStateByTick(int tick)
        {
            var state = history[tick%historyLength];
            if (state != null && state.Tick == tick)
                return state;
            return null;
        }
    }


    public class GameState
    {
        public int Tick { get; set; }
        public IList<Unit> Players { get; set; }
        public IList<ShotInfo> Shots { get; set; }
    }
}
