﻿using System.Collections.Generic;

namespace exception_server.Model.Maps
{
    internal class Wall
    {
        public Wall()
        {
            Tiles = new List<Tile>();
        }

        public bool Equals(Wall wall)
        {

            if (Lenght != wall.Lenght)
                return false;
            return ((Tiles[0] == wall.Tiles[0] && Tiles[Lenght - 1] == wall.Tiles[wall.Lenght - 1])
                || (Tiles[Lenght - 1] == wall.Tiles[0] && Tiles[0] == wall.Tiles[wall.Lenght - 1]));
        }

        public int Lenght
        {
            get { return Tiles.Count; }
        }
        public IList<Tile> Tiles { get; set; }
    }
}
