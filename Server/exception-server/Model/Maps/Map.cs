﻿//using Newtonsoft.Json;
using System;
using exception_server.Config;
using exception_server.Model.Gameplay;

namespace exception_server.Model.Maps
{
    [Serializable]
    public class Map 
    {
        public Tile[,] Tiles { get; private set; }
        public string SerializedVersion { get; set; }

        public Map()
        {
            Tiles = new Tile[MapConfig.MapWidth, MapConfig.MapHeight];
            for (int i = 0; i < MapConfig.MapWidth; i++)
            {
                for (int j = 0; j < MapConfig.MapHeight; j++)
                {
                    Tiles[i, j] = new Tile(i, j, Tile.TileType.Groung, DirectionEnum.Up);
                }
            }
        }

        public Tile this[int x, int y]
        {
            get { return Tiles[x, y]; }
            set { Tiles[x, y] = value; }
        }
    }
}
