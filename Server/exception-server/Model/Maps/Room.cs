﻿using System;
using System.Collections.Generic;

namespace exception_server.Model.Maps
{

    internal class Neighbor
    {
        public Room OrigRoom { get; set; }
        public Room NeighRoom { get; set; }
        public Wall CommonWall { get; set; }
        public bool DoorExists { get; set; }
    }

    internal class Room
    {
        private static int _idCounter;

        public Room()
        {
            Id = _idCounter++;
        }
        public int Id { get; private set; }
        public int Left { get; set; }
        public int Top { get; set; }
        public int Right
        {
            get { return Left + Width - 1; }
        }

        public int Bottom
        {
            get { return Top + Height - 1; }
        }

        public int Area
        {
            get { return Height * Width; }
        }

        //for BFS
        public int Mark { get; set; }

        public List<Neighbor> Neighbors { get; set; }

        public List<Wall> Walls { get; set; }

        public void CalculateWalls(Map map)
        {
            Walls = new List<Wall> { new Wall(), new Wall(), new Wall(), new Wall() };
            for (int i = Left; i <= Right; i++)
                Walls[0].Tiles.Add(map[i, Top]);
            for (int j = Top; j <= Bottom; j++)
                Walls[1].Tiles.Add(map[Right, j]);
            for (int i = Right; i >= Left; i--)
                Walls[2].Tiles.Add(map[i, Bottom]);
            for (int j = Bottom; j >= Top; j--)
                Walls[3].Tiles.Add(map[Left, j]);
        }

        public int Width { get; set; }
        public int Height { get; set; }
        public int WindowsCount { get; set; }
        public int DoorsCount { get; set; }
        public bool IsPartOfCorridor { get; set; }

        public Boolean CheckIfTileBelongsToRoom(Tile tile)
        {
            return Left <= tile.X && Top <= tile.Y && Right >= tile.X && Bottom >= tile.Y;
        }
    }
}
