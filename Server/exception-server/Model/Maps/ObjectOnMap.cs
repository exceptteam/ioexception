﻿//using Newtonsoft.Json;
using System;
using exception_server.Model.Gameplay;

namespace exception_server.Model.Maps
{
    
    [Serializable]
    public class ObjectOnMap
    {
        public enum ObjectType
        { 
	        DamagedCar = 0,
            Tree = 1,
            RectangleTable1 = 2

        };

        public ObjectType Type { set; get; }

        public DirectionEnum Direction { set; get; }

        public float Width { set; get; }
        public float Height { set; get; }
    }
}
