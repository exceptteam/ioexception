﻿using System;
using System.Collections.Generic;
using System.Linq;
using exception_server.Config;
using exception_server.Model.Gameplay;
using TileType = exception_server.Model.Maps.Tile.TileType;

namespace exception_server.Model.Maps
{
    public class MapGenerator : Loggable
    {
        public MapGenerator()
        {
            Random = new Random(DateTime.Now.Millisecond);
            Map = new Map();
        }

        public Map Map { get; private set;  }

        protected int MapHeight
        {
            get { return MapConfig.MapHeight; }
        }

        protected int MapWidth
        {
            get { return MapConfig.MapWidth; }
        }

        public Random Random { get; set; }

        /// <summary>
        /// Generates one part of the global map. Contains the main logic of map generation
        /// </summary>
        public Map GenerateMap()
        {
            GenerateTempMap();
            return Map;
        }

        public void GenerateTempMap()
        {
            Log.Info("Started temp map generation");
            GenerateVerticalWall(0, 0, 100, DirectionEnum.Left);
            GenerateHorizontalWall(0, 0, 100, DirectionEnum.Up);
            GenerateHorizontalWall(0, 99, 100, DirectionEnum.Down);
            GenerateVerticalWall(99, 0, 100, DirectionEnum.Right);
            GenerateVerticalRoad(60, 1, 6, 98);
            GenerateHorizontalRoad(1, 48, 9, 98);
            GenerateComplexBuilding(3, 3, 53, 42, 40 + Random.Next(20));
            GenerateComplexBuilding(3, 60, 53, 30, 40 + Random.Next(20));
            GenerateSquareComplexBuilding(70, 3, 25, 43);
            GenerateComplexBuilding(68, 60, 27, 35, 40 + Random.Next(20));
        }

        private void GenerateSquareComplexBuilding(int left, int top, int width, int height)
        {
            GenerateComplexBuilding(left, top, width, height, 100);
        }


        internal void GenerateHorizontalRoad(int left, int top, int height, int lenght)
        {
            int bottom = top + height - 1;

            for (int x = left; x < left + lenght; x++)
            {
                Map[x, top].Fill(TileType.RoadEdge, DirectionEnum.Up);
                Map[x, bottom].Fill(TileType.RoadEdge, DirectionEnum.Down);
                for (int y = 1; y < height - 1; y++)

                    Map[x, y + top].Fill((y % 4 == 0 && height - y > 4) ? TileType.RoadDelimiter : TileType.Road, DirectionEnum.Left);
            }
        }

        internal void GenerateVerticalRoad(int left, int top, int width, int lenght)
        {
            int right = left + width - 1;

            for (int y = top; y < top + lenght; y++)
            {
                Map[left, y].Fill(TileType.RoadEdge, DirectionEnum.Left);
                Map[right, y].Fill(TileType.RoadEdge, DirectionEnum.Right);
                for (int x = 1; x < width - 1; x++)

                    Map[x + left, y].Fill((x % 4 == 0 && width - x > 4) ? TileType.RoadDelimiter : TileType.Road, DirectionEnum.Up);
            }
        }

        /// <summary>
        /// Generates buildings in specified area that occupy specified percentage of that area.
        /// </summary>
        public void GenerateComplexBuilding(int left, int top, int width, int height, int occupyPercentsOfArea)
        {
            if (width < 3)
                throw new ArgumentOutOfRangeException("width", "Width of area for generation of complex buildings must be greater than 2");

            if (height < 3)
                throw new ArgumentOutOfRangeException("height", "Height of area for generation of complex buildings must be greater than 2");

            //all room holders
            var roomHolders = GenerateRoomHolders(left, top, width, height);
            //holders outside buildings
            List<Room> emptyHolders = roomHolders.ToList();
            List<Room> rooms;
            if (occupyPercentsOfArea == 100)
            {
                rooms = roomHolders;
                foreach (var room in rooms)
                    RenderRoom(room);
            }
            else
            {
                // - 10% it is compensation for MakeBuildingsMoreSquared witch decrease count of rooms in building
                int minCountOfTilesToOccupy = (occupyPercentsOfArea - 10) * width * height / 100;
                rooms = GenerateBuildingRooms(roomHolders, minCountOfTilesToOccupy, emptyHolders);

                MakeBuildingsMoreSquared(emptyHolders, rooms);
                MakeBuildingsMoreSquared(emptyHolders, rooms);

                RemoveSmallBuildings(emptyHolders, rooms);
            }

            BuildRoomsGraph(rooms);

            MakeCorridors(rooms);

            MakeInnerDoors(rooms);

            MakeOuterDoors(rooms);

            FixWallsAndCorners(left, top, width, height);
        }

       
        private List<Room> GenerateBuildingRooms(List<Room> roomHolders, int minCountOfTilesToOccupy, List<Room> emptyHolders)
        {
            var rooms = new List<Room>();

            
            int curBuildingArea = 0;
            int numberOfAttempts = 0;
            var queuedRoomsForProcessing = new Queue<Room>();

            //added -10 because of MakeBuildingsMoreSquared logic

            while ((curBuildingArea < minCountOfTilesToOccupy && numberOfAttempts < roomHolders.Count) ||
                   queuedRoomsForProcessing.Count > 0)
            {
                Room room;
                if (queuedRoomsForProcessing.Count > 0)
                {
                    room = queuedRoomsForProcessing.Dequeue();
                    roomHolders.Remove(room);
                }
                else
                {
                    int indexOfRoomForProcessing = Random.Next(emptyHolders.Count);
                    room = emptyHolders[indexOfRoomForProcessing];
                }

                numberOfAttempts++;
                curBuildingArea += room.Width*room.Height;

                RenderRoom(room);
                //Context.GetService<Serializer>().PrintMapToLog();
                CheckIfNeighboursMustBeAdded(room.Left, room.Top, queuedRoomsForProcessing, emptyHolders);
                CheckIfNeighboursMustBeAdded(room.Left, room.Bottom, queuedRoomsForProcessing, emptyHolders);
                CheckIfNeighboursMustBeAdded(room.Right, room.Bottom, queuedRoomsForProcessing, emptyHolders);
                CheckIfNeighboursMustBeAdded(room.Right, room.Top, queuedRoomsForProcessing, emptyHolders);

                emptyHolders.Remove(room);
                rooms.Add(room);
            }
            return rooms;
        }

        internal List<Room> GenerateRoomHolders(int left, int top, int width, int height)
        {
            if (width < 3)
                throw new ArgumentOutOfRangeException("width", "Width of area for generation of room holders must be greater than 2");

            if (height < 3)
                throw new ArgumentOutOfRangeException("height", "Height of area for generation of room holders must be greater than 2");

            var firstRoom = new Room
            {
                Left = left,
                Top = top,
                Height = height,
                Width = width
            };
            var roomHolders = new List<Room>();
            GenerateRoomHoldersRecursive(firstRoom, roomHolders);

            return roomHolders;
        }
        
        private void GenerateRoomHoldersRecursive(Room room, List<Room> result)
        {
            const int minCut = 6;
            const int minLenForCut = minCut * 2 - 1;
            if (room.Area < minLenForCut * minCut || (room.Area < 180 && Random.Next(room.Area) < 35))
            {
                result.Add(room);
                return;
            }

            var newRoom = new Room();

            var isHorizontal = Random.Next(1000) >= 500;
            if ((isHorizontal || room.Width < minLenForCut) && room.Height >= minLenForCut)
            {
                int randomDiapasone = room.Height - minLenForCut;

                int cut = minCut + (randomDiapasone > 0 ? Random.Next(randomDiapasone) : 0);
                newRoom.Width = room.Width;
                newRoom.Height = room.Height - cut + 1;
                newRoom.Top = room.Top + cut - 1;
                newRoom.Left = room.Left;

                room.Height = cut;
            }
            else if (room.Width >= minLenForCut)
            {
                int randomDiapasone = room.Width + 1 - 2 * minCut;
                int cut = minCut + (randomDiapasone > 0 ? Random.Next(randomDiapasone) : 0);
                newRoom.Width = room.Width - cut + 1;
                newRoom.Height = room.Height;
                newRoom.Top = room.Top;
                newRoom.Left = room.Left + cut - 1;

                room.Width = cut;
            }
            else
            {
                result.Add(room);
                return;
            }
            GenerateRoomHoldersRecursive(room, result);
            GenerateRoomHoldersRecursive(newRoom, result);
        }

        private void MakeOuterDoors(List<Room> processedRooms)
        {
            foreach (var room in processedRooms)
                room.Mark = 0;

            //connect rooms that does not touch corridors
            foreach (var root in processedRooms)
            {
                //get separate building
                if (root.Mark == 0)
                {
                    var buildingRooms = new List<Room>();
                    root.Mark = 1;
                    var q = new Queue<Room>();
                    q.Enqueue(root);
                    while (q.Count > 0)
                    {
                        Room current = q.Dequeue();
                        buildingRooms.Add(current);
                        foreach (var neigh in current.Neighbors.Where(n => n.NeighRoom.Mark == 0 && n.DoorExists))
                        {
                            var pairedRoom = neigh.NeighRoom;
                            pairedRoom.Mark = 1;
                            q.Enqueue(pairedRoom);
                        }
                    }

                    int outerDoorsNecessary = Math.Max(((int)Math.Sqrt(buildingRooms.Count) - 1), 1);
                    int doorsMade = 0;
                    int iterations = 0;
                    while (doorsMade < outerDoorsNecessary && iterations < buildingRooms.Count * 4)
                    {
                        iterations++;
                        var room = buildingRooms[Random.Next(1000 % buildingRooms.Count)];
                        if (room.Walls.Where(w => w.Lenght > 4).Any(wall => MakeDoor(room, wall, true)))
                        {
                            doorsMade++;
                        }
                    }
                }
            }
        }

        private void MakeInnerDoors(IList<Room> processedRooms)
        {
            int corridorNum = 0;

            //connect corridors
            foreach (var root in processedRooms.Where(r => r.IsPartOfCorridor))
            {
                if (root.Mark == 0)
                {
                    corridorNum++;
                    root.Mark = corridorNum;
                    var q = new Queue<Room>();
                    q.Enqueue(root);
                    while (q.Count > 0)
                    {
                        Room current = q.Dequeue();

                        foreach (var neigh in current.Neighbors
                                .Where(r => r.NeighRoom.Mark == 0 && r.DoorExists))
                        {
                            var pairedRoom = neigh.NeighRoom;
                            pairedRoom.Mark = corridorNum;
                            q.Enqueue(pairedRoom);
                        }
                    }
                }
            }

            foreach (var room in processedRooms.Where(r => r.IsPartOfCorridor))
            {
                Room room1 = room;
                foreach (var neigh in room.Neighbors.Where(n => n.NeighRoom.IsPartOfCorridor))
                {
                    Neighbor neigh1 = neigh;
                    if (neigh1.NeighRoom.Mark != room1.Mark)
                    {
                        MakeInnerDoor(room, neigh1);
                        var oldMark = neigh1.NeighRoom.Mark;
                        foreach (var corrRoom in processedRooms.Where(r => r.IsPartOfCorridor && r.Mark == oldMark))
                        {
                            corrRoom.Mark = room1.Mark;
                        }
                        break;
                    }
                }
            }
            foreach (var room in processedRooms.Where(r => r.IsPartOfCorridor))
                room.Mark = 0;

            //connect rooms to corridors
            foreach (var corr in processedRooms.Where(r => r.IsPartOfCorridor))
            {
                foreach (var neigh in corr.Neighbors
                    .Where(r => r.NeighRoom.Mark == 0 && r.NeighRoom.IsPartOfCorridor == false))
                {
                    var pairedRoom = neigh.NeighRoom;
                    if (MakeInnerDoor(corr, neigh))
                    {
                        pairedRoom.Mark = 1;
                    }
                }
            }

            //connect rooms that does not touch corridors
            foreach (var root in processedRooms.Where(r => !r.IsPartOfCorridor))
            {
                //conncet rooms
                if (root.Mark == 0)
                {
                    root.Mark = 1;
                    var q = new Queue<Room>();
                    q.Enqueue(root);
                    while (q.Count > 0)
                    {
                        Room current = q.Dequeue();

                        foreach (var neigh in current.Neighbors)
                        {
                            var pairedRoom = neigh.NeighRoom;
                            if (MakeInnerDoor(current, neigh))
                            {
                                if (pairedRoom.Mark == 0)
                                    q.Enqueue(pairedRoom);
                                pairedRoom.Mark = 1;
                                break;
                            }
                        }
                    }
                }
            }
        }

        private bool MakeDoor(Room room, Wall wall, bool isOuterDoor)
        {
            if (wall.Lenght <= 3 || wall.Tiles.Any(t => t.Type == TileType.Door))
                return false;
            var placesForDoor = new List<Tuple<Tile, Tile>>();
            for (int index = 1; index < wall.Lenght - 1; index++)
            {
                var door1Tile = wall.Tiles[index];
                var door2Tile = wall.Tiles[index + 1];

                if (isOuterDoor)
                {
                    if (door1Tile.Type == TileType.WideWall && IsOuterWallTile(door1Tile) &&
                        door2Tile.Type == TileType.WideWall && IsOuterWallTile(door2Tile))
                        placesForDoor.Add(Tuple.Create(door1Tile, door2Tile));
                }
                else 
                    if (door1Tile.Type == TileType.WideWall && door2Tile.Type == TileType.WideWall)
                        placesForDoor.Add(Tuple.Create(door1Tile, door2Tile));
                }

            if (placesForDoor.Count <= 0)
                return false;

            var placeForDoor = placesForDoor[Random.Next(1000) % placesForDoor.Count];

            placeForDoor.Item1.Type = TileType.Door;
            placeForDoor.Item2.Type = TileType.Door;
            room.DoorsCount++;
            return true;
        }

        private bool MakeInnerDoor(Room room, Neighbor neighbor)
        {
            var pairedRoom = neighbor.NeighRoom;
            var wall = neighbor.CommonWall;
            if (!neighbor.DoorExists && MakeDoor(room, wall, false))
            {
                pairedRoom.DoorsCount++;
                neighbor.DoorExists = true;
                var symmetricConnection = pairedRoom.Neighbors.FirstOrDefault(n => n.NeighRoom.Id == room.Id);
                if (symmetricConnection != null)
                    symmetricConnection.DoorExists = true;
                return true;
            }
            return false;
        }

        private void MakeCorridors(List<Room> processedRooms)
        {
            int corrCount = 0;
           
            var orderedRooms = processedRooms.OrderByDescending(x => x.Neighbors.Count);
            foreach (var room in orderedRooms)
            {
                if (!room.IsPartOfCorridor)
                {
                    corrCount += ExpandCorridor(room);
                    if (corrCount > (processedRooms.Count / 8))
                        break;
                }
            }            
        }

        private int ExpandCorridor(Room room)
        {
            var pairedNeigh = room.Neighbors
                .Where(neigh => neigh.DoorExists == false &&
                                neigh.CommonWall.Lenght <= 7 && neigh.CommonWall.Lenght > 4 &&
                                room.Walls.Exists(wall => wall.Equals(neigh.CommonWall)))
                .OrderByDescending(x => x.NeighRoom.Neighbors.Count)
                .FirstOrDefault();
            if (pairedNeigh != null)
            {
                var pairedRoom = pairedNeigh.NeighRoom;
                for (int index = 1; index < pairedNeigh.CommonWall.Tiles.Count - 1; index++)
                {
                    var tile = pairedNeigh.CommonWall.Tiles[index];
                    tile.Type = TileType.RoomFloor;
                }
                //recalculate walls
                room.CalculateWalls(Map);
                //recalculate neighbours
                pairedRoom.DoorsCount++;
                room.DoorsCount++;
                pairedNeigh.DoorExists = true;
                var symmetricConnection = pairedRoom.Neighbors.FirstOrDefault(n => n.NeighRoom.Id == room.Id);
                if (symmetricConnection != null)
                    symmetricConnection.DoorExists = true;
                room.IsPartOfCorridor = true;
                pairedRoom.IsPartOfCorridor = true;
                if (Random.Next(100) < 60)
                    return 1 + ExpandCorridor(room) + ExpandCorridor(pairedRoom);
                return 1 + ExpandCorridor(room);
            }
            return 0;
        }

        private void BuildRoomsGraph(List<Room> processedRooms)
        {
            foreach (var room in processedRooms)
            {
                 room.Neighbors = GetRoomNeighbors(processedRooms, room);
            }
        }

        private List<Neighbor> GetRoomNeighbors(List<Room> processedRooms, Room room)
        {
            var neighbors = new Dictionary<int, Neighbor>();
            room.CalculateWalls(Map);
            var walls = room.Walls;
            foreach (var wall in walls)
            {
                var neighRooms = FindWallNeighbours(room, processedRooms, wall);
                foreach (var neighRoom in neighRooms)
                {
                    var commonWall = new Wall {Tiles = wall.Tiles.Where(t => neighRoom.CheckIfTileBelongsToRoom(t)).ToList()};
                    var neighbor = new Neighbor {CommonWall = commonWall, OrigRoom = room, NeighRoom = neighRoom};
                    if (!neighbors.ContainsKey(neighRoom.Id))
                        neighbors.Add(neighRoom.Id, neighbor);
                }
            }
            return neighbors.Values.ToList();
        }

        private IEnumerable<Room> FindWallNeighbours(Room room, List<Room> processedRooms, Wall wall)
        {
            if (wall.Lenght < 3)
                return new List<Room>();

            var minX = Math.Min(wall.Tiles[2].X, wall.Tiles[wall.Lenght - 3].X);
            var minY = Math.Min(wall.Tiles[2].Y, wall.Tiles[wall.Lenght - 3].Y);
            var width = Math.Abs(wall.Tiles[2].X - wall.Tiles[wall.Lenght - 3].X);
            var height = Math.Abs(wall.Tiles[2].Y - wall.Tiles[wall.Lenght - 3].Y);
            var neighTiles = GetTilesOnRectangle(minX - 1, minY - 1, width + 3, height + 4);

            var neighRooms = new Dictionary<int, Room>();
            foreach (var tile in neighTiles)
            {
                if (!room.CheckIfTileBelongsToRoom(tile))
                {
                    var neigh = GetRoomByTile(tile, processedRooms);
                    if (neigh != null && !neighRooms.ContainsKey(neigh.Id))
                        neighRooms.Add(neigh.Id, neigh);
                }
            }
            return neighRooms.Values;
        }

        private static Room GetRoomByTile(Tile tile, IEnumerable<Room> rooms)
        {
            return rooms.FirstOrDefault(r => r.CheckIfTileBelongsToRoom(tile));
        }

        private void RenderRoom(Room room)
        {
            GenerateSquareBuilding(room.Left, room.Top, room.Width, room.Height);
        }

        private void MakeBuildingsMoreSquared(IList<Room> freeRooms, IList<Room> processedRooms)
        {
            foreach (var room in freeRooms.ToList())
            {
                var surroundings = GetRoomSurroundings(room);
                var notGroundCount = surroundings.Count(t => t.Type != TileType.Groung);
                if (notGroundCount >= (room.Height + room.Width) * (0.7f + (float)Random.Next(80) / 100))
                {
                    RenderRoom(room);
                    freeRooms.Remove(room);
                    processedRooms.Add(room);
                }
            }
        }

        private void RemoveSmallBuildings(IList<Room> freeRooms, IList<Room> processedRooms)
        {
            foreach (var room in processedRooms.ToArray())
            {
                if (room.Area > 130) continue;
                var surroundings = GetRoomSurroundings(room);
                var groundCount = surroundings.Count(t => t.Type == TileType.Groung);
                if (groundCount >= (room.Height + room.Width)*2 + 2)
                {
                    GenerateRectangle(room.Left, room.Top, room.Width, room.Height, TileType.Groung);
                    processedRooms.Remove(room);
                }
                else if (groundCount >= (room.Height + room.Width) * 2 - 2)
                {
                    var firstEmpty = surroundings.FirstOrDefault(t => t.Type == TileType.Groung);
                    if (firstEmpty != null && !firstEmpty.Equals(default(Tile)))
                    {
                        Room roomToAdd = GetRoomByTile(firstEmpty, freeRooms);
                        if (roomToAdd == null)
                            return;
                        RenderRoom(roomToAdd);
                        processedRooms.Add(roomToAdd);
                    }
                }
            }
        }

        private void CheckIfNeighboursMustBeAdded(int x, int y, Queue<Room> queuedRoomsForProcessing, IList<Room> freeRooms)
        {
            var surroundings = GetTilesOnRectangle(x - 1, y - 1, 3, 3);
            int cornersCount = surroundings.Count(t => t.Type == TileType.WallCorner);
            int wallsCount = surroundings.Count(t => t.Type == TileType.WideWall);
            if (cornersCount > 0 || wallsCount >= 4)
            {
                var firstEmpty = surroundings.FirstOrDefault(t => t.Type == TileType.Groung);
                if (firstEmpty != null && !firstEmpty.Equals(default(Tile)))
                {
                    Room room = GetRoomByTile(firstEmpty, freeRooms);
                    if (room == null)
                        return;
                    queuedRoomsForProcessing.Enqueue(room);
                    freeRooms.Remove(room);
                }
            }
        }

        private bool IsWallOrDoor(TileType type)
        {
            return type == TileType.Door || type == TileType.Wall || type == TileType.WallCorner || type == TileType.WideWall;
        }

        private bool IsOuterWallTile(Tile tile)
        {
            return CountCellNeighbours(tile, TileType.Groung) > 0;
        }
       

        private void FixWallsAndCorners(int left, int top, int width, int height)
        {
            for (int x = left; x < left + width; x++)
                for (int y = top; y < top + height; y++)
                {
                    var tile = Map[x, y];
                    if (tile.Type != TileType.WideWall && tile.Type != TileType.WideWallCorner)
                        continue;

                    tile.Type = IsOuterWallTile(tile) ? TileType.WideWall : TileType.Wall;
                    if (CountCellNeighbours(tile, TileType.RoomFloor) == 8)
                        tile.Type = TileType.RoomFloor;
                }

            for (int x = left; x < left + width; x++)
                for (int y = top; y < top + height; y++)
                {
                    var tile = Map[x, y];
                    if (tile.Type != TileType.Wall)
                        continue;

                    var leftTile = Map[x - 1, y].Type;
                    var upperTile = Map[x, y - 1].Type;

                    if (IsWallOrDoor(leftTile))
                    {
                        if (IsWallOrDoor(upperTile))
                        {
                            tile.Type = TileType.WallCorner;
                            tile.Direction = DirectionEnum.Right;
                        }
                        else
                        {
                            tile.Direction = DirectionEnum.Down;
                        }
                    }
                    else if (IsWallOrDoor(upperTile))
                    {
                        tile.Direction = DirectionEnum.Right;
                    }
                    else if (IsWallOrDoor(Map[x + 1, y].Type) && IsWallOrDoor(Map[x, y + 1].Type))
                        tile.Type = TileType.SmallCorner;
                }
        }

        //Counts all free cells around of specified
        private int CountCellNeighbours(Tile tile, TileType type)
        {
            var x = tile.X;
            var y = tile.Y;
            if (x < 1 || y < 1 || x > MapWidth - 1 || y > MapHeight - 1)
                throw new ArgumentOutOfRangeException();

            int count = 0;
            for (int offsetX = -1; offsetX <= 1; offsetX++)
            {
                for (int offsetY = -1; offsetY <= 1; offsetY++)
                {
                    //if ((Math.Abs(offsetX) + Math.Abs(offsetY) != 1))
                    //    continue;

                    if (offsetX == 0 && offsetY == 0)
                        continue;

                    int currentX = x + offsetX;
                    int currentY = y + offsetY;

                    if (Map[currentX, currentY].Type == type)
                    {
                        count++;
                    }
                }
            }
            return count;
        }

        internal void GenerateSquareBuilding(int left, int top, int width, int height)
        {
            int bottom = top + height - 1;
            int right = left + width - 1;
            GenerateRectangle(left + 1, top + 1, width - 2, height - 2, TileType.RoomFloor);
            GenerateVerticalWall(left, top, height, DirectionEnum.Right);
            GenerateVerticalWall(right, top, height, DirectionEnum.Right);
            GenerateHorizontalWall(left + 1, top, width - 2, DirectionEnum.Down);
            GenerateHorizontalWall(left + 1, bottom, width - 2, DirectionEnum.Down);
            Map[left, top].Fill(TileType.WideWallCorner, DirectionEnum.Right);
            Map[right, top].Fill(TileType.WideWallCorner, DirectionEnum.Right);
            Map[right, bottom].Fill(TileType.WideWallCorner, DirectionEnum.Right);
            Map[left, bottom].Fill(TileType.WideWallCorner, DirectionEnum.Right);
        }

        private void GenerateWideWallPiece(int x, int y, DirectionEnum direction)
        {
            var tile = Map[x, y];
            if (tile.Type != TileType.WideWallCorner)
                tile.Fill(TileType.WideWall, direction);
        }

        internal void GenerateHorizontalWall(int left, int top, int width, DirectionEnum direction)
        {
            for (int i = left; i < left + width; i++)
                GenerateWideWallPiece(i, top, direction);
        }

        internal void GenerateVerticalWall(int left, int top, int height, DirectionEnum direction)
        {
            for (int j = top; j < top + height; j++)
               GenerateWideWallPiece(left, j, direction);
        }

        internal void GenerateRectangle(int left, int top, int width, int height, TileType type)
        {
            for (int i = left; i < left + width; i++)
                for (int j = top; j < top + height; j++)
                    Map[i, j].Fill(type, DirectionEnum.Up);
        }

        private IList<Tile> GetRoomSurroundings(Room room)
        {
            return GetTilesOnRectangle(room.Left - 1, room.Top - 1, room.Width + 2, room.Height + 2);
        }

        private IList<Tile> GetTilesOnRectangle(int left, int top, int width, int height)
        {
            int bottom = top + height - 1;
            int right = left + width - 1;
            var res = new List<Tile>();
            for (int i = left; i <= right; i++)
                res.Add(Map[i, top]);
            for (int j = top + 1; j <= bottom; j++)
                res.Add(Map[right, j]);
            for (int i = right - 1; i >= left; i--)
                res.Add(Map[i, bottom]);
            for (int j = bottom - 1; j > top; j--)
                res.Add(Map[left, j]);
            return res;
        }
    }
}
