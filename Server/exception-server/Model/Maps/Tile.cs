﻿//using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using exception_server.Model.Gameplay;
using exception_server.Model.Gameplay.Collisions;

namespace exception_server.Model.Maps
{
    /// <summary>
    /// The rectangular cell of map, that represents part of wall of floor (or another object on a map), and also provices an 
    /// information about creatures (units) or items placed in that cell of map.
    /// </summary>
    [Serializable]
    public class Tile
    {
        public Tile(int x, int y)
        {
            X = x;
            Y = y;
        }
        public Tile(int x, int y, TileType type, DirectionEnum direction) : this(x, y)
        {
            Type = type;
            Direction = direction;
        }

        public int X;
        public int Y;

        public void Fill(TileType type, DirectionEnum direction)
        {
            Type = type;
            Direction = direction;
        }

        public  enum TileType
        { 
	        RoomFloor  = 0, 
            Groung = 1,
            Road = 2,
            RoadEdge = 3,
            RoadTurn = 4,
	        Wall = 5,
            WallCorner = 6,
            WideWall = 7,
            WideWallCorner = 8,
            Grass = 9,
            Door = 10,
            RoadDelimiter = 11,//road with traffic line markings
            SmallCorner = 12,
        };

        public TileType Type { set; get; }

        /* public enum DirectionEnum
           {
               Up = 0,
               Right = 1,
               Down = 2,
               Left = 3
           }*/
        public DirectionEnum Direction { set; get; }

        protected IList<IArea> NonWalkableAreas;

        public string Serialize()
        {
            return ((char) (((uint) Type << 2) + (uint)Direction)).ToString();
        }

        public bool ColidesWith(Circle playerCircle)
        {
            if (NonWalkableAreas == null)
                NonWalkableAreas = CalculateNonWalkableAreas();
            return NonWalkableAreas.Any(nonWalkableArea => nonWalkableArea.OverlapsWith(playerCircle));
        }

        private const float THIN_WALL_WIDTH = 0.5f;

        private IList<IArea> CalculateNonWalkableAreas()
        {
            var nonWalkableAreas = new List<IArea>();
            switch (Type)
            {
                case TileType.Wall:
                    switch (Direction)
                    {
                        case DirectionEnum.Up:
                            nonWalkableAreas.Add(new Rectangle(new FloatCoords(X, Y), 1, THIN_WALL_WIDTH));
                            break;
                        case DirectionEnum.Right:
                            nonWalkableAreas.Add(new Rectangle(new FloatCoords(X + 1 - THIN_WALL_WIDTH, Y), THIN_WALL_WIDTH, 1));
                            break;
                        case DirectionEnum.Down:
                            nonWalkableAreas.Add(new Rectangle(new FloatCoords(X, Y + 1 - THIN_WALL_WIDTH), 1, THIN_WALL_WIDTH));
                            break;
                        case DirectionEnum.Left:
                            nonWalkableAreas.Add(new Rectangle(new FloatCoords(X, Y), THIN_WALL_WIDTH, 1));
                            break;
                        case DirectionEnum.NoDirection:
                            break;
                    }
                    break;
                case TileType.WallCorner:
                    switch (Direction)
                    {
                        case DirectionEnum.Up:
                            nonWalkableAreas.Add(new Rectangle(new FloatCoords(X, Y), 1, THIN_WALL_WIDTH));
                            nonWalkableAreas.Add(new Rectangle(new FloatCoords(X + 1 - THIN_WALL_WIDTH, Y), THIN_WALL_WIDTH, 1));
                            break;
                        case DirectionEnum.Right:
                            nonWalkableAreas.Add(new Rectangle(new FloatCoords(X + 1 - THIN_WALL_WIDTH, Y), THIN_WALL_WIDTH, 1));
                            nonWalkableAreas.Add(new Rectangle(new FloatCoords(X, Y + 1 - THIN_WALL_WIDTH), 1, THIN_WALL_WIDTH));
                            break;
                        case DirectionEnum.Down:
                            nonWalkableAreas.Add(new Rectangle(new FloatCoords(X, Y), THIN_WALL_WIDTH, 1));
                            nonWalkableAreas.Add(new Rectangle(new FloatCoords(X, Y + 1 - THIN_WALL_WIDTH), 1, THIN_WALL_WIDTH));
                            break;
                        case DirectionEnum.Left:
                            nonWalkableAreas.Add(new Rectangle(new FloatCoords(X, Y), THIN_WALL_WIDTH, 1));
                            nonWalkableAreas.Add(new Rectangle(new FloatCoords(X + 1 - THIN_WALL_WIDTH, Y), THIN_WALL_WIDTH, 1));
                            break;
                        case DirectionEnum.NoDirection:
                            break;
                    }
                    break;
                 
                case TileType.WideWall:
                    nonWalkableAreas.Add(new Rectangle(new FloatCoords(X, Y), 1, 1));
                    break;
            }
            return nonWalkableAreas;
        }
    }
}
