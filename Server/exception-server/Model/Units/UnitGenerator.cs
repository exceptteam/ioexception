﻿using System;
using System.Collections.Generic;
using exception_server.Config;

namespace exception_server.Model.Units
{
    public class UnitGenerator
    {
      
        /// <summary>
        /// Generates unit
        /// </summary>
        public static void GenerateUnitCharacteristics(Unit unit)
        {
            unit.Id = GenerateId();
            unit.Params = GenerateParams(unit.Type);
            unit.CurHp = unit.MaxHp;
        }


        private static IDictionary<ParamsEnum, int> GenerateParams(UnitType type)
        {
            return UnitConfig.ParamsByType[type];
        }

        private static string GenerateId()
        {
            return Guid.NewGuid().ToString();
        }

        public static Unit GenerateRobot(UnitType type)
        {
            var robot = new Unit
            {
                Type = type
            };
            GenerateUnitCharacteristics(robot);
            return robot;
        }
    }
}
