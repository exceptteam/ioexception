﻿using System.Collections.Generic;
using exception_server.Config;
using exception_server.Model.Gameplay;
using exception_server.Model.Gameplay.Collisions;
using Newtonsoft.Json;

namespace exception_server.Model.Units
{
    /// <summary>
    /// Describes units and contains logic for generation and manipulation them.
    /// </summary>
    [JsonObject(MemberSerialization.OptOut)]
    public class Unit
    {
        public Unit()
        {
            Params = new Dictionary<ParamsEnum, int>();
        }

        public string Id { get; set; }

        [JsonIgnore]
        public UnitType Type { get; set; }

        public int Kills { get; set; }
        public int Deaths { get; set; }
        public float X { get; set; }
        public float Y { get; set; }

        [JsonIgnore]
        public FloatCoords Coords { get { return new FloatCoords(X, Y); } }

        public MoveDirection Direction { get; set; }
        [JsonIgnore]
        public IDictionary<ParamsEnum, int> Params { get; set; }


        public float GetUnitRadius()
        {
            return ((float)Params[ParamsEnum.Radius])/1000;
        }

        public float GetSpeed()
        {
            return (GameConfig.SpeedFactor * Params[ParamsEnum.Speed] * GameConfig.TickDuration) / 1000;
        }

        private string name;
        private int curHp;

        public string Name
        {
            get
            {
                return string.IsNullOrEmpty(name)
                    ? Type.ToString().Replace("_", " ")
                    : name;
            }
            set { name = value; }
        }

        public int CurHp
        {
            get { return curHp; }
            set { curHp = value < 0 ? 0 : value; }
        }

        public int MaxHp
        {
            get { return Params[ParamsEnum.MaxHp]; }
            set { Params[ParamsEnum.MaxHp] = value; }
        }

        public double Angle { get; set; }

        [JsonIgnore]
        public bool IsDead
        {
            get { return CurHp <= 0; }
        }

        [JsonIgnore]
        public int TickOfLastShot { get; set; }
}

    public enum ParamsEnum
    {
        MaxHp = 0,
        CurHp,
        MeleeMastery,
        Speed,
        Radius,
    }

    public enum UnitType
    {
        //this creatures cant be enemies
        MaleHuman = 0,
        FemaleHuman = 1,
        CleanerBot = 2,
        DeliveryDron = 3
    }
}
