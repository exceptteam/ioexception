﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Timers;
using exception_server.Communication;
using exception_server.Config;
using exception_server.Model.Gameplay.Collisions;
using exception_server.Model.Maps;
using exception_server.Model.Units;
using Timer = System.Timers.Timer;

namespace exception_server.Model.Gameplay
{
    public class Game : Loggable
    {
        private readonly Object syncLock = new Object();
        private Timer timer;
        private int tick;
        private readonly List<ChatMessage> chatMessages = new List<ChatMessage>();
        protected NetworkingService NetworkingService { get; set; }
        protected Serializer Serializer { get; set; }
        public Map Map { get; set; }
        public List<Client> Clients { get; set; }
        public List<Client> ActiveClients { get; set; }

        IList<ShotInfo> ShotCommands  { get; set; }

        protected GameHistory GameHistory { get; set; }

        protected GameState CurGameState { get; set; }

        public Game()
        {
            Clients = new List<Client>();
            ActiveClients = new List<Client>();
            NetworkingService = new NetworkingService();
            Serializer = new Serializer();
            GameHistory = new GameHistory();
        }

        public void Start()
        {
            Log.Info("Started new game");
            var mapService = new MapGenerator();
            Map = mapService.GenerateMap();
            Map.SerializedVersion = Serializer.SerializeMap(Map);
            NetworkingService.StartListening();
            NetworkingService.ClientConnected += ClientConnected;

            timer = new Timer(GameConfig.TickDuration);
            timer.Elapsed += CalculatStep;
            timer.Start();
        }

        void CalculatStep(object sender, ElapsedEventArgs e)
        {
            if (!Monitor.TryEnter(syncLock))
                return;

            try
            {
                tick++;
                ShotCommands = new List<ShotInfo>();
                CurGameState = new GameState{Tick = tick, Shots = new List<ShotInfo>()};
                GameHistory.SaveGameState(CurGameState);
                
                foreach (var clientHandler in Clients.ToList())
                {
                    try
                    {
                        while (true)
                        {
                            var command = clientHandler.RecieveCommand();
                            if (command == null)
                            {
                                if (tick - clientHandler.LastAnswerOnTick > GameConfig.PingTimeout)
                                {
                                    clientHandler.TcpHandler.Disconnect();
                                    throw new PlayerDisconnnectedException("Not answered too long");
                                }
                                break;
                            }
                            clientHandler.LastAnswerOnTick = tick;

                            HandleCommand(command, clientHandler);
                        }
                    }
                    catch (PlayerDisconnnectedException)
                    {
                        ActiveClients.Remove(clientHandler);
                        Clients.Remove(clientHandler);
                        Log.InfoFormat("Player {0} disconnected",
                            clientHandler.Player == null ? "" : clientHandler.Player.Id);
                    }
                }
                // GetService<AIService>().EnemyTurn();

                foreach (var clientHandler in ActiveClients)
                {
                    if (clientHandler.Player != null)
                        MovePlayer(clientHandler.Player);
                }

                CurGameState.Players = GetPlayers();

                foreach (var shotInfo in ShotCommands)
                {
                    HandleShot(shotInfo);
                }

                var gameStateWithDelay = GameHistory.GetGameStateByTick(tick - GameConfig.PingDelay);
                if (gameStateWithDelay != null)
                    foreach (var client in ActiveClients)
                        SendResponseToClient(client, gameStateWithDelay);
                
                chatMessages.Clear();
            }
            finally
            {
                Monitor.Exit(syncLock);
            }
        }

        private void HandleShot(ShotInfo shotInfo)
        {
            var tickOfShot = TimeStampRoundToTick(shotInfo.TimeStamp);
            if (tickOfShot <= tick - GameConfig.PingDelay)
                tickOfShot = tick - GameConfig.PingDelay;
            if (tickOfShot > tick)
                tickOfShot = tick;
            var gameStateForShotTick = GameHistory.GetGameStateByTick(tickOfShot);

            var shooter = gameStateForShotTick.Players.FirstOrDefault(p => p.Id == shotInfo.PlayerId);

            if (shooter == null)
                return;

            if (shooter.TickOfLastShot > tickOfShot - GameConfig.LaserShotReload / GameConfig.TickDuration)
                return;

            shooter.Angle = CalculateAngleOfVector(shotInfo.From, shotInfo.To);
            shooter.TickOfLastShot = tickOfShot;

            int damage = 0;
            string shotVictimId = string.Empty;
            gameStateForShotTick.Shots.Add(shotInfo);
            if (CheckShot(gameStateForShotTick, shotInfo, ref damage, ref shotVictimId))
            {
                if (shotVictimId == shotInfo.PlayerId)
                    return;
                shotInfo.Damage = damage;
                for (int t = tickOfShot; t <= tick; t++)
                {
                    var victim = GameHistory.GetGameStateByTick(t).Players.FirstOrDefault(p => p.Id == shotVictimId);
                    if (victim != null)
                    {
                        victim.CurHp -= damage;
                        if (victim.CurHp <= 0)
                            HandleDeath(shooter, victim);
                    }
                }
            }
        }

        private void HandleDeath(Unit shooter, Unit victim)
        {
            shooter.Kills ++;
            victim.Deaths ++;
        }

        private bool CheckShot(GameState gameStateForShotTick, ShotInfo shotInfo, ref int damage, ref string shotVictimId)
        {
            var shotCircle = new Circle {Center = shotInfo.To, Radius = 0.2f};
            foreach (var player in gameStateForShotTick.Players)
            {
                var playerCircle = new Circle { Center = player.Coords, Radius = player.GetUnitRadius() };
                if (shotCircle.OverlapsWith(playerCircle))
                {
                    shotVictimId = player.Id;
                    damage = 2;

                    if (CheckHeadShot(shotInfo, player))
                    {
                        damage = 4;
                        var firstOrDefault = GetPlayers().FirstOrDefault(p => p.Id == shotInfo.PlayerId);
                        if (firstOrDefault != null)
                            chatMessages.Add(new ChatMessage
                            {
                                Message = string.Format(": {0} shot {1}", firstOrDefault.Name, player.Name),
                                SenderId = "",
                                SenderName = "Headshot"
                            });
                    }

                    return true;
                }
            }
            return false;
        }

        private bool CheckHeadShot(ShotInfo shotInfo, Unit player)
        {
            float shotVectorX = shotInfo.To.X - shotInfo.From.X;
            float shotVectorY = shotInfo.To.Y - shotInfo.From.Y;
            var len2 = (float) Math.Sqrt(shotVectorX*shotVectorX + shotVectorY*shotVectorY);
            var headShotCircle = new Circle
            {
                Center = new FloatCoords(
                    shotInfo.To.X + (shotVectorX/len2*0.7f),
                    shotInfo.To.Y + (shotVectorY/len2*0.7f)),
                Radius = 0.1f
            };
            var playerHead = new Circle {Center = player.Coords, Radius = player.GetUnitRadius()/4};
            if (headShotCircle.OverlapsWith(playerHead)) //headshot!
            {
                return true;
            }
            return false;
        }

        private void HandleCommand(ClientCommand command, Client clientHandler)
        {
            Unit player = clientHandler.Player;
            if (player != null && player.IsDead)
            {
                if (command.Command == CommandType.ReadyToRespawn)
                    Respawn(player);
                return;
            }
            switch (command.Command)
            {
                case CommandType.Disconnect:
                    ClientLeavesGame(clientHandler);
                    break;
                case CommandType.JoinGame:
                    ClientJoinsGame((JoinGameCommand) command, clientHandler);
                    clientHandler.Player = ((JoinGameCommand) command).Player;
                    break;
                case CommandType.MoveStart:
                    if (player == null)
                        Log.DebugFormat("Recieved move start command when player is null");
                    else
                        HandleMoveCommand((MoveStartCommand)command, player);
                    break;
                case CommandType.ShotInfo:
                    var shot = (ShotInfoCommand) command;
                    ShotCommands.Add(new ShotInfo
                    {
                        From = new FloatCoords(shot.BegX, shot.BegY),
                        To = new FloatCoords(shot.EndX, shot.EndY),
                        PlayerId = clientHandler.Player.Id,
                        TimeStamp = shot.TimeStamp
                    });
                    break;
                case CommandType.ChatMessage:
                    if (player == null)
                        Log.DebugFormat("Recieved chat command when player is null");
                    else
                    {
                        var message = (ChatMessageCommand) command;

                        chatMessages.Add(new ChatMessage
                        {
                            Message = message.Message,
                            SenderId = player.Id,
                            SenderName = player.Name
                        });
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException("command",
                        string.Format("Command {0} is not implemented", command.Command));
            }
        }

        public void StopGame()
        {
             timer.Stop();
             NetworkingService.StopListening();
             Log.Info("Server is stopped, game is finished.");
        }

        private void SendResponseToClient(Client client, GameState gameState)
        {
            var response = new ServerResponse{ TimeStamp = TickToTimeStamp(tick) };
            if (chatMessages.Count > 0)
                response.ChatMessages = chatMessages;

            response.Player = client.Player;
            response.Enemies = gameState.Players.Where(p => p != client.Player);
            response.Shots = gameState.Shots.Where(s => s.PlayerId != client.Player.Id);

            client.TcpHandler.Send(Serializer.SerializeResponse(response));
        }

        private int TickToTimeStamp(int tick_)
        {
            return tick_ * GameConfig.TickDuration;
        }


        private int TimeStampRoundToTick(int timeStamp)
        {
            return Convert.ToInt32((float)timeStamp / GameConfig.TickDuration);
        }

        private List<Unit> GetPlayers()
        {
            return ActiveClients.Select(ac => ac.Player).ToList();
        }

        public void ClientConnected(object sender, ClientConnectedEventArgs e)
        {
            lock (syncLock)
            {
                Log.Info("New client connected.");
                Clients.Add(e.Client);
                e.Client.LastAnswerOnTick = tick;
            }
        }

        private void SendMapToClient(Client client)
        {
            Log.DebugFormat("Sent map to new client");

            client.TcpHandler.Send(Map.SerializedVersion);
        }


        public void ClientJoinsGame(JoinGameCommand command, Client client)
        {
            if (Clients.Count < GameConfig.MaxPlayersCount)
            {
                Respawn(command.Player);
                Log.DebugFormat("Generated player character for player {0}", command.Player.Id);
                ActiveClients.Add(client);
                SendMapToClient(client);
            }
        }

        private void Respawn(Unit player)
        {
            player.Direction = MoveDirection.None;
            UnitGenerator.GenerateUnitCharacteristics(player);
            GeneratePlayerPosition(player);
        }

        private void GeneratePlayerPosition(Unit player)
        {
            var random = new Random(DateTime.Now.Millisecond);
            
            while (true)
            {
                player.X = random.Next(MapConfig.MapWidth - 3) + 1.5f;
                player.Y = random.Next(MapConfig.MapHeight - 3) + 1.5f;
                if (CheckNoCollisions(player.X, player.Y, player))
                    break;
            }
        }
        
        private void ClientLeavesGame(Client client)
        {
            client.TcpHandler.Disconnect();
            throw new PlayerDisconnnectedException("Client sent command to leave game.");
        }


        public void HandleMoveCommand(MoveStartCommand command, Unit player)
        {
            player.Direction = command.Direction;
        }
        
        public void MovePlayer(Unit player)
        {
            if (player.Direction != MoveDirection.None && !player.IsDead)
            {
                var direction = new Direction(player.Direction);

                //even if there is collision, player angle should be "direction of movement".
                SetUnitAngle(player, CalculateAngleOfVector(player.Coords, GetNewCoords(player, direction)));

                if (!TryToMovePlayer(player, direction))
                    if (!TryToMovePlayer(player,  direction.TurnClockwise(1)))
                        if (!TryToMovePlayer(player, direction.TurnClockwise(-1)))
                            if (!TryToMovePlayer(player, direction.TurnHalfClockwise(3)))
                                TryToMovePlayer(player, direction.TurnHalfClockwise(-3));//player-player sliding
            }
        }

        private void SetUnitAngle(Unit player, double angle)
        {
            if (player.TickOfLastShot < tick - GameConfig.RememberAngleAfterShot / GameConfig.TickDuration)
            {
                player.Angle = angle;
            }
        }

        private static double CalculateAngleOfVector(FloatCoords vectorStart, FloatCoords vectorEnd)
        {
            return Math.Atan2(vectorEnd.Y - vectorStart.Y, vectorEnd.X - vectorStart.X) * 180 / Math.PI;
        }

        private bool TryToMovePlayer(Unit player, Direction direction)
        {
            FloatCoords newCoorsd = GetNewCoords(player, direction);
            if (CheckNoCollisions(newCoorsd.X, newCoorsd.Y, player))
            {
                SetUnitAngle(player, CalculateAngleOfVector(player.Coords, newCoorsd));
                player.X = newCoorsd.X;
                player.Y = newCoorsd.Y;

                return true;
            }
            return false;
        }

        private static FloatCoords GetNewCoords(Unit player, Direction direction)
        {
            float speed = player.GetSpeed();
            FloatCoords newCoorsd;

            newCoorsd.X = player.X + direction.OffsetX * speed;
            newCoorsd.Y = player.Y + direction.OffsetY * speed;
            return newCoorsd;
        }

        private bool CheckNoCollisions(float newX, float newY, Unit player)
        {
            var playerCircle = new Circle { Center = new FloatCoords(newX, newY), Radius = player.GetUnitRadius() };
            
            for (int offsetX = -1; offsetX <= 1; offsetX++)
            {
                for (int offsetY = -1; offsetY <= 1; offsetY++)
                {
                    int currentX = (int)newX + offsetX;
                    int currentY = (int)newY + offsetY;

                    if (currentX >= 0 && currentY >= 0 && currentX < MapConfig.MapWidth && currentY < MapConfig.MapHeight 
                        && Map[currentX, currentY].ColidesWith(playerCircle))
                    {
                        return false;
                    }
                }
            }

            return (from client in ActiveClients
                where client.Player.Id != player.Id && !client.Player.IsDead
                select new Circle
                {
                    Center = client.Player.Coords, Radius = client.Player.GetUnitRadius()
                }).All(secondCircle => !playerCircle.OverlapsWith(secondCircle));
        }
    }
}
