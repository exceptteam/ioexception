﻿using System;

namespace exception_server.Model.Gameplay.Collisions
{
    public class Circle : IArea
    {
        public FloatCoords Center { get; set; }
        public float Radius { get; set; }


        public bool OverlapsWith(IArea anotherArea)
        {
            if (anotherArea is Rectangle)
                return (anotherArea as Rectangle).OverlapsWith(this);
            if (anotherArea is Circle)
            {
                var anotherCircle = anotherArea as Circle;
                return (Math.Pow(Center.X - anotherCircle.Center.X, 2f) + Math.Pow(Center.Y - anotherCircle.Center.Y, 2f) 
                        < Math.Pow(Radius + anotherCircle.Radius, 2));
            }
            throw new NotImplementedException();
        }
    }
}
