﻿using System;

namespace exception_server.Model.Gameplay.Collisions
{
    public class Rectangle : IArea
    {
        public Rectangle(FloatCoords topLeft, float width, float height)
        {
            TopLeft = topLeft;
            BottomRight = new FloatCoords(topLeft.X + width, topLeft.Y + height);
        }
        public FloatCoords TopLeft { get; set; }
        public FloatCoords BottomRight { get; set; }

        public float Width {
            get { return BottomRight.X - TopLeft.X; }
        }

        public float Height
        {
            get { return BottomRight.Y - TopLeft.Y; }
        }

        public bool OverlapsWith(IArea anotherArea)
        {
            if (anotherArea is Circle)
                return OverlapsWithCircle(anotherArea as Circle);
            throw new NotImplementedException();
        }

        private bool OverlapsWithCircle(Circle circle)
        {
            float discriminant = 0;
            var center = circle.Center;
            // for axes x and y
            for (int i = 0; i < 2; i++)
            {
                // if circle center is before or after rectangle on that axe
                if (center[i] < TopLeft[i] || center[i] > BottomRight[i])
                {
                    
                    //calculate square of distance on that axe 
                    float distance = center[i] - ((center[i] < TopLeft[i]) ? TopLeft[i] : BottomRight[i]);
                    discriminant += distance * distance;
                }
            }

            return discriminant <= (circle.Radius * circle.Radius);
        }
    }
}
