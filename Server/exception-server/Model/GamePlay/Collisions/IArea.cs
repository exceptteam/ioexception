﻿namespace exception_server.Model.Gameplay.Collisions
{
    public interface IArea
    {
        bool OverlapsWith(IArea anotherArea);
    }
}
