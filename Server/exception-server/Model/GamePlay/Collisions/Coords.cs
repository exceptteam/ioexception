﻿namespace exception_server.Model.Gameplay.Collisions
{
    public struct FloatCoords
    {
        public float X;
        public float Y;

        public FloatCoords(float x, float y)
        {
            X = x;
            Y = y;
        }

        public float this[int coordNum]
        {
            get { return coordNum == 0 ? X : Y; }
            set
            {
                if (coordNum == 0)
                    X = value;
                else
                    Y = value;
            }
        } 
    }

     public struct IntCoords
    {
        public int X;
        public int Y;

        public IntCoords(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}
