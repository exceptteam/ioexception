﻿using System;
using System.Collections.Generic;
using exception_server.Model.Gameplay.Collisions;

namespace exception_server.Model.Gameplay
{
    public enum DirectionEnum
    {
        Up = 0,
        Right = 1,
        Down = 2,
        Left = 3,
        NoDirection = 4
    }
    public enum MoveDirection
    {
		None = 0,
		North = 1,
		NE = 2,
		East = 3,
		SE = 4,
		South = 5,
		SW = 6,
		West = 7,
		NW = 8
    }

    public class Direction
    {
        protected bool Equals(Direction other)
        {
            return dirEnum == other.dirEnum;
        }

        public override int GetHashCode()
        {
            return dirEnum;
        }

        public override bool Equals(object obj)
        {
            return dirEnum.Equals((int)((Direction)obj).DirectionEnum);
        }

        protected Direction(){}

        public Direction(float offsetX, float offsetY)
        {
            OffsetX = offsetX;
            OffsetY = offsetY;
            var dirIndex = directions.FindIndex(d => Math.Abs(d.X - offsetX) < 0.00001f && Math.Abs(d.Y - offsetY) < 0.00001f);
            if (dirIndex < 0)
                dirIndex = 0; //dirty hack for TurnHalfClockwise

            dirEnum = dirIndex;
        }

        public Direction(MoveDirection dirEnum_)
        {
            dirEnum = (int)dirEnum_;
            OffsetX = directions[dirEnum].X;
            OffsetY = directions[dirEnum].Y;
        }

        public Direction TurnClockwise(int times)
        {
            var ofsets = directions[(dirEnum + 7 + times)%8 + 1];
            return new Direction(ofsets.X, ofsets.Y);
        }

        public Direction TurnHalfClockwise(int times)
        {
            var dir = TurnClockwise(times/2);
            if (times%2 != 0)
            {
                var nextDir = dir.TurnClockwise(times > 0 ? 1 : -1);
                return new Direction((dir.OffsetX + nextDir.OffsetX) / 2, (dir.OffsetY + nextDir.OffsetY) / 2);
            }
            return dir;
        }

        public float OffsetX { get; set; }
        public float OffsetY { get; set; }

        private readonly int dirEnum;// DirectionEnum to int
        public MoveDirection DirectionEnum
        {
            get { return (MoveDirection)dirEnum; }
        }

        private const float DIAG_SPEED = 1 / 1.41421356f;

        private static readonly List<FloatCoords> directions = new List<FloatCoords>
        {
            new FloatCoords(0, 0), //none
            new FloatCoords(0, -1), //north
            new FloatCoords(DIAG_SPEED, -DIAG_SPEED), //ne
            new FloatCoords(1, 0), //east
            new FloatCoords(DIAG_SPEED, DIAG_SPEED), //se
            new FloatCoords(0, 1), //south
            new FloatCoords(-DIAG_SPEED, DIAG_SPEED), //sw
            new FloatCoords(-1, 0), //west
            new FloatCoords(-DIAG_SPEED, -DIAG_SPEED) //nw
        };
    }
}
