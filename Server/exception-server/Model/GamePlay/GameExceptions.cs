﻿using System;

namespace exception_server.Model.Gameplay
{
    public class PlayerDiedException: ApplicationException
    {
    }

    public class GameFinishedException : ApplicationException
    {
    }

    public class PlayerDisconnnectedException : ApplicationException
    {
        public PlayerDisconnnectedException(string message) : base(message)
        {
        }
    }
}
