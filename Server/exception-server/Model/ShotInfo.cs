﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using exception_server.Model.Gameplay.Collisions;
using Newtonsoft.Json;

namespace exception_server.Model
{
    [JsonObject(MemberSerialization.OptOut)]
    public class ShotInfo
    {
        [JsonIgnore]
        public int TimeStamp { get; set; }
        public string PlayerId { get; set; }
        public FloatCoords From { get; set; }
        public FloatCoords To { get; set; }
        public int Damage { get; set; }
    }
}
