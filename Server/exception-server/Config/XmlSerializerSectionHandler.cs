﻿// A big thank you to Craig Andera for this one!
// http://staff.develop.com/candera/weblog/stories/2003/02/20/theLastConfigurationSectionHandlerIllEverNeed.html
// Author: Fabrice Marguerie
// http://weblogs.asp.net/fmarguerie/
// fabrice@madgeek.com
//
// Free for use
//
// Based on code from Fritz Onion: http://pluralsight.com/blogs/fritz/archive/2004/07/21/1651.aspx
//

using System;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;

namespace exception_server.Config
{
	/// <summary>
	/// Configuration section handler that deserializes connfiguration settings to an object.
	/// </summary>
	/// <remarks>The configuration node must have a type attribute defining the type to deserialize to.</remarks>
	public class XmlSerializerSectionHandler : IConfigurationSectionHandler
	{
		/// <summary>
		/// Implemented by all configuration section handlers to parse the XML of the configuration section.
		/// The returned object is added to the configuration collection and is accessed by <see cref="ConfigurationManager.GetSection"/>.
		/// </summary>
		/// <param name="parent">The configuration settings in a corresponding parent configuration section.</param>
		/// <param name="section">The <see cref="XmlNode"/> that contains the configuration information from the configuration file. Provides direct access to the XML contents of the configuration section.</param>
		/// <returns>A configuration object.</returns>
		public object Create(object parent, object configContext, XmlNode section)
		{
			XPathNavigator navigator = section.CreateNavigator();
			var typeName = (string)navigator.Evaluate("string(@type)");
			Type type = Type.GetType(typeName, true);
			var serializer = new XmlSerializer(type);
			return serializer.Deserialize(new XmlNodeReader(section));
		}
	}
}
