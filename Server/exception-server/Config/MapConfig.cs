﻿using System.Configuration;
using System.Xml.Serialization;

namespace exception_server.Config
{
    [XmlRoot("mapSettings")]
	public class MapConfigEntity
	{
        [XmlElement("mapWidth")]
        public int MapWidth { get; set; }

        [XmlElement("mapHeight")]
        public int MapHeight { get; set; }
		
	}

    public static class MapConfig
    {
		private static readonly MapConfigEntity configEntity;

        static MapConfig()
        {
            configEntity = (MapConfigEntity)ConfigurationManager.GetSection("mapSettings");
        }

        public static int MapWidth
        {
            get
            {
                return configEntity.MapWidth;
            }
            //set is used for unit tests. Maybe later it will be replaced by using DependencyInjections.
            set { configEntity.MapWidth = value; }
        }

        public static int MapHeight
        {
            get
            {
                return configEntity.MapHeight;
            }
            //set is used for unit tests. Maybe later it will be replaced by using DependencyInjections.
            set { configEntity.MapHeight = value; }
        }
	}
}
