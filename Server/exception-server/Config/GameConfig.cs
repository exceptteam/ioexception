﻿using System.Configuration;
using System.Xml.Serialization;

namespace exception_server.Config
{
    [XmlRoot("gameSettings")]
	public class GameConfigEntity
	{
        [XmlElement("mapLogFile")]
        public string MapLogFile { get; set; }


        [XmlElement("tickDuration")]
        public int StepDuration { get; set; } 
        
        [XmlElement("maxPlayersCount")]
        public int MaxPlayersCount { get; set; }

        [XmlElement("pingDelay")]
        public int PingDelay { get; set; }

        [XmlElement("ticksCountToRemember")]
        public int StepsCountToRemember { get; set; }

        [XmlElement("speedFactor")]
        public float SpeedFactor { get; set; }

        [XmlElement("serverPort")]
        public int ServerPort { get; set; }

        [XmlElement("pingTimeout")]
        public int PingTimeout { get; set; }

        [XmlElement("laserShotReload")]
        public int LaserShotReload { get; set; }

        [XmlElement("rememberAngleAfterShot")]
        public int RememberAngleAfterShot { get; set; }
	}

    public static class GameConfig
    {
		private static readonly GameConfigEntity configEntity;

        static GameConfig()
        {
            configEntity = (GameConfigEntity)ConfigurationManager.GetSection("gameSettings");
        }

        public static string MapLogFile
        {
            get
            {
                return configEntity.MapLogFile;
            }
          
        }

        /// <summary>
        /// Duration of one "tick" in ms. Tick = one iteration of server life-cycle
        /// </summary>
        public static int TickDuration { get { return configEntity.StepDuration; } }

        public static int MaxPlayersCount { get { return configEntity.MaxPlayersCount; } }

        public static int TicksCountToRemember { get { return configEntity.StepsCountToRemember; } }

        public static int PingDelay { get { return configEntity.PingDelay; } }

        /// <summary>
        /// Speed of player with Speed param = 1 in tiles per second
        /// </summary>
        public static float SpeedFactor { get { return configEntity.SpeedFactor; } }

        public static int ServerPort { get { return configEntity.ServerPort; } }
        public static int PingTimeout { get { return configEntity.PingTimeout; } }
        public static int RememberAngleAfterShot { get { return configEntity.RememberAngleAfterShot; } }
        public static int LaserShotReload { get { return configEntity.LaserShotReload; } }
    }
}
