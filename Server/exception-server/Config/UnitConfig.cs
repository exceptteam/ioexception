﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Xml.Serialization;
using exception_server.Model.Units;

namespace exception_server.Config
{
    [XmlRoot("unitSettings")]
    public class UnitConfigEntity
    {
        [XmlElement("types")]
        public TypesList TypesList { get; set; }
    }

    public class TypesList
    {
        [XmlElement("type")]
        public List<ParamsConfig> Types { get; set; }
    }

    public class ParamsConfig
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlElement("maxHp")]
        public int MaxHp { get; set; }

        [XmlElement("meleeMastery")]
        public int MeleeMastery { get; set; }

        [XmlElement("speed")]
        public int Speed { get; set; }

        [XmlElement("unitRadius")]
        public int UnitRadius { get; set; }
    }


    public static class UnitConfig
	{
        private static readonly UnitConfigEntity unitConfigEntity;

	    static UnitConfig()
	    {
            unitConfigEntity = (UnitConfigEntity)ConfigurationManager.GetSection("unitSettings");
        }

        public static IDictionary<UnitType, IDictionary<ParamsEnum, int>> ParamsByType
        {
            get
            {
                var paramsByType = new Dictionary<UnitType, IDictionary<ParamsEnum, int>>();
                foreach (var config in unitConfigEntity.TypesList.Types)
                {
                    var type = (UnitType)Enum.Parse(typeof(UnitType), config.Name);
                    var typeParamsStats = GetParamsFromConfig(config);
                    paramsByType.Add(type, typeParamsStats);

                }
                return paramsByType;
            }
        }

        private static IDictionary<ParamsEnum, int> GetParamsFromConfig(ParamsConfig config)
        {
            var oneRaceStats = new Dictionary<ParamsEnum, int>
                {
                    {ParamsEnum.MaxHp, config.MaxHp},
                    {ParamsEnum.MeleeMastery, config.MeleeMastery},
                    {ParamsEnum.Speed, config.Speed},
                    {ParamsEnum.Radius, config.UnitRadius}
                };
            return oneRaceStats;

        }

        public static int UnitTypesCount
        {
            get
            {
                return unitConfigEntity.TypesList.Types.Count;
            }
        }
	}
}
