﻿using log4net;

namespace exception_server
{
    public abstract class Loggable
    {
        private ILog log;
        protected ILog Log
        {
            get { return log ?? (log = LogManager.GetLogger(GetType())); }
        }
    }
}
