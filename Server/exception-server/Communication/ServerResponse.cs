﻿using System.Collections.Generic;
using exception_server.Model;
using exception_server.Model.Units;
using log4net.Core;

namespace exception_server.Communication
{
    public class ServerResponse
    {
        public int TimeStamp { get; set; }
        public IEnumerable<ChatMessage> ChatMessages { get; set; }
        public Unit Player { get; set; }
        public IEnumerable<Unit> Enemies { get; set; }
        public IEnumerable<ShotInfo> Shots { get; set; }
    }

    public struct ChatMessage
    {
        public string SenderName { get; set; }
        public string SenderId { get; set; }
        public string Message { get; set; }
    }
}
