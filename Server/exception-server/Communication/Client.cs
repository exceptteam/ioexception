﻿using System;
using System.Net.Sockets;
using exception_server.Model.Gameplay;
using exception_server.Model.Units;

namespace exception_server.Communication
{

    public class Client : Loggable
    {
        public Client(Socket socket)
        {
            tcpHandler = new TcpHandler(socket);
            serializer = new Serializer();
        }

        public Unit Player { get; set; }

        private static Serializer serializer;
        public Serializer Serializer {
            get { return serializer; }
        }

        public TcpHandler TcpHandler
        {
            get { return tcpHandler; }
        }

        public int LastAnswerOnTick { get; set; }

        private readonly TcpHandler tcpHandler;

        public ClientCommand RecieveCommand()
        {
            ClientCommand result;
            try
            {
                var request = TcpHandler.Recieve();
                if (string.IsNullOrEmpty(request))
                    return null;
                Log.DebugFormat("Recieved cliend command: {0}", request);
                result = Serializer.DeserializeRequestCommand(request);
            }
            catch (SocketException ex)
            {
                Log.Info("Player was disconnected because of exception in Socket.Recieve", ex);
                throw new PlayerDisconnnectedException(ex.Message);
            }
            return result;
        }
    }

    public delegate void ClientConnectedEventHandler(object sender, ClientConnectedEventArgs e);

    /// <summary>
    /// The class that contains information about received command.
    /// </summary>
    public class ClientConnectedEventArgs : EventArgs
    {
        public Client Client { get; set; }

        /// <summary>
        /// Creates an instance of CommandEventArgs class.
        /// </summary>
        /// <param name="cmd">The received command.</param>
        public ClientConnectedEventArgs(Client client)
        {
            this.Client = client;
        }
    }
}
