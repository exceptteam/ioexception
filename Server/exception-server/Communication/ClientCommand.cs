﻿using exception_server.Model.Gameplay;
using exception_server.Model.Units;

namespace exception_server.Communication
{
    public enum CommandType
    {
        NoCommand,
        JoinGame,
        ReadyToRespawn, 
        Disconnect,
        ShotInfo,
        MoveStart,
        ChatMessage,
    }

    public class ClientCommand
    {
        //public int PlayerTocken { get; set; }
        public int TimeStamp { get; set; }
        public CommandType Command { get; set; }
    }

    public class JoinGameCommand : ClientCommand
    {
        public Unit Player { get; set; }
    }


    public class ChatMessageCommand : ClientCommand
    {
        public string Message { get; set; }
    }

    public class MoveStartCommand : ClientCommand
    {
        public MoveDirection Direction { get; set; }
    }

    public class ShotInfoCommand : ClientCommand
    {
        public float BegX { get; set; }
        public float BegY { get; set; }
        public float EndX { get; set; }
        public float EndY { get; set; }
    }
}
