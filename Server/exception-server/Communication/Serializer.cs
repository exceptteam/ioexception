﻿using System;
using System.IO;
using System.Text;
using exception_server.Model.Gameplay;
using exception_server.Model.Maps;
using Newtonsoft.Json;
using exception_server.Config;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace exception_server.Communication
{
    public class Serializer : Loggable
    {
        private static readonly JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    StringEscapeHandling = StringEscapeHandling.Default,
                };
        public string SerializeResponse(ServerResponse resp)
        {
            //return SerializeMap(Context.Map);
            try
            {
                var res = JsonConvert.SerializeObject(resp, Formatting.Indented, settings);
                return res;
            }
            catch (JsonException ex)
            {
                Log.Error(string.Format("Serialization of response failed with exception. Response = {0}", resp), ex);
            }
            return string.Empty;
        }

      
        public static string SerializeMap(Map map)
        {
            var s = new StringBuilder();
            for (var i = 0; i < MapConfig.MapWidth; i++)
            {
                for (var j = 0; j < MapConfig.MapHeight; j++)
                {
                    var tile = map[i, j];

                    s.Append(tile.Serialize());
                }
            }
            return s.ToString();
        }

        //For example,  PrintMap(@"D:\1\1\map.txt")
        public void PrintMap(Map map, string filePath)
        {
            var writer = new StreamWriter(filePath, false, Encoding.Unicode);
            writer.Write(SerializeMapForLog(map));
            writer.Close();
        }

        public void PrintMapToLog(Map map)
        {
            PrintMap(map, GameConfig.MapLogFile);
        }

        public string SerializeMapForLog(Map map)
        {
            var builder = new StringBuilder();

            for (var i = 0; i < MapConfig.MapWidth; i++)
            {
                for (var j = 0; j <  MapConfig.MapHeight; j++)
                {
                    var tile = map[j, i];
                    builder.Append(GetTileCharForLog(tile));
                }
                builder.AppendLine();
            }
            return builder.ToString();
        }

        private static string GetTileCharForLog(Tile tile)
        {
            switch (tile.Type)
            {
                case Tile.TileType.RoadDelimiter:
                     switch (tile.Direction)
                    {
                        case DirectionEnum.Up:
                        case DirectionEnum.Down:
                            return "┊┊";
                        case DirectionEnum.Right:
                        case DirectionEnum.Left:
                            return "==";
                    }
                    break;
               
                case Tile.TileType.Wall:
                    switch (tile.Direction)
                    {
                        case DirectionEnum.Up:
                            return "▀▀";
                        case DirectionEnum.Down:
                            return "▄▄";
                        case DirectionEnum.Right:
                            return " █";
                        case DirectionEnum.Left:
                            return "█ ";    
                        
                    }
                    break;
                case Tile.TileType.SmallCorner:
                    return " ▄";
                case Tile.TileType.WideWall:
                case Tile.TileType.WideWallCorner:
                    return "██";
                    
                case Tile.TileType.WallCorner:
                    switch (tile.Direction)
                    {
                        case DirectionEnum.Right:
                            return "▄█";   
                         case DirectionEnum.Up:
                             return "-┐";
                        case DirectionEnum.Down:
                            return "L-";
                        case DirectionEnum.Left:
                            return "┌-";
                    }
                    break;
                case Tile.TileType.Road:
                    return "░░";
                case Tile.TileType.RoadEdge:
                    switch (tile.Direction)
                    {
                        case DirectionEnum.Up:
                        case DirectionEnum.Down:
                            return "──";
                        case DirectionEnum.Right:
                            return "░]";
                        case DirectionEnum.Left:
                            return "[░";
                    }
                    break;
                case Tile.TileType.RoomFloor:
                    return " .";
                case Tile.TileType.Door:
                    return "\\\\";
            }
            return "  ";
        }

        public string SerializePlayerDetails()
        {
            throw new NotImplementedException();
        }

        public string SerializeMenuState()
        {
            throw new NotImplementedException();
        }

        public ClientCommand DeserializeRequestCommand(string request)
        {
            try
            {
                JObject req = JObject.Parse(request);
                CommandType commandType;
                var parseSucceded = Enum.TryParse((string)req["Command"], true, out commandType);
                if (parseSucceded)
                {
                    //there can't be an Exception is deserializing because whole request was deserialized correctly
                    switch (commandType)
                    {
                        case CommandType.NoCommand:
                            return null;
                        case CommandType.JoinGame:
                            return JsonConvert.DeserializeObject<JoinGameCommand>(request);
                        case CommandType.MoveStart:
                            return JsonConvert.DeserializeObject<MoveStartCommand>(request);
                        case CommandType.ChatMessage:
                            return JsonConvert.DeserializeObject<ChatMessageCommand>(request);
                        case CommandType.ShotInfo:
                            return JsonConvert.DeserializeObject<ShotInfoCommand>(request);
                        default:
                            return JsonConvert.DeserializeObject<ClientCommand>(request);
                    }
                }
                return null;
            }
            catch (JsonException ex)
            {
                Log.Error(string.Format("Deserialization of request failed with exception. Request = {0}", request.Length < 500 ? request : request.Substring(0, 500)), ex);
                return null;
            }
        }
    }
}
