using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace exception_server.Communication
{
    public class TcpHandler : Loggable
    {
        public Socket Socket { get; set; }
        private StringBuilder data = new StringBuilder();
        private int bytesRec;

        public TcpHandler(Socket socket)
        {
            Socket = socket;
            Socket.NoDelay = true;
        }

        public string Recieve()
        {
            if (Socket.Blocking)
            {
                while (data.Length == 0 || data.ToString().IndexOf("<EOF>", StringComparison.Ordinal) == -1)
                {
                    var bytes = new byte[1024];
                    bytesRec = Socket.Receive(bytes, 1024, SocketFlags.None);
                    data.Append(Encoding.ASCII.GetString(bytes, 0, bytesRec));
                    if (bytesRec == 0)
                        return null;
                }
            }
            else
            {
                if (data.Length == 0 || data.ToString().IndexOf("<EOF>", StringComparison.Ordinal) == -1)
                {
                    // An incoming connection needs to be processed.
                    while (Socket.Poll(0, SelectMode.SelectRead))
                    {
                        var bytes = new byte[1024];
                        bytesRec = Socket.Receive(bytes, 1024, SocketFlags.None);
                        data.Append(Encoding.ASCII.GetString(bytes, 0, bytesRec));
                        if (bytesRec == 0)
                            return null;
                    }
                }
                if (data.Length <= 0)
                    return null;
            }

            int indexOfEof = data.ToString().IndexOf("<EOF>", StringComparison.Ordinal);
            if (indexOfEof > -1)
            {
                var result = data.ToString(0, indexOfEof);
                int nextCommandIndex = indexOfEof + "<EOF>".Length;
                if (data.Length <= nextCommandIndex) 
                    data = new StringBuilder(string.Empty); 
                else data.Remove(0, nextCommandIndex);
                return result;
            }
            return null;
        }

        public void Send(string message)
        {
            byte[] msg = Encoding.ASCII.GetBytes(message + "<EOF>");

            try
            {
                Socket.Send(msg);
            }
            catch (SocketException ex)
            {
                Log.Error("Error in send response", ex);
            }
        }

        /// <summary>
        /// Disconnect the current client manager from the remote client and returns true if the client had been disconnected from the server.
        /// </summary>
        /// <returns>True if the remote client had been disconnected from the server,otherwise false.</returns>
        public void Disconnect()
        {
            if (Socket != null && Socket.Connected)
            {
                try
                {
                    Socket.Shutdown(SocketShutdown.Both);
                    Socket.Close();
                }
                catch (SocketException ex)
                {
                    Log.Error("Error in socket disconnect", ex);
                }
            }
            if (Socket == null)
                Log.InfoFormat("Tried disconnect socket that is null");
            else
            {
                try
                {
                    var ipEndPoint = Socket.RemoteEndPoint as IPEndPoint;
                    if (ipEndPoint != null)
                        Log.InfoFormat("Tried disconnect socket that is already disconnected. Socket address = {0}",
                            ipEndPoint.Address);
                    return;
                }
                catch (ObjectDisposedException)
                {
                    Log.Info("Tried disconnect socket that is already disposed.");
                }
                Log.Info("Tried disconnect socket that is already disconnected.");
            }
        }
    }
}