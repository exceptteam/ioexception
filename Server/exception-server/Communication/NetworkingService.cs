﻿using System;
using System.ComponentModel;
using System.Net;
using System.Net.Sockets;
using exception_server.Config;

namespace exception_server.Communication
{
    public class NetworkingService : Loggable
    {
        public NetworkingService()
        {
            serverPort = GameConfig.ServerPort;
            serverIP = IPAddress.Any;
        }
        
        private Socket listenerSocket;
        private readonly IPAddress serverIP;
        private readonly int serverPort;
        private BackgroundWorker bwListener;

        /// <summary>
        /// Occurs when a command received from a remote client.
        /// </summary>
        public event ClientConnectedEventHandler ClientConnected;

        protected virtual void OnClientConnected(ClientConnectedEventArgs e)
        {
            ClientConnectedEventHandler handler = ClientConnected;
            if (handler != null) handler(this, e);
        }

        public void StartListening()
        {
            Console.WriteLine("Waiting for a connection...");

            bwListener = new BackgroundWorker {WorkerSupportsCancellation = true};
            bwListener.DoWork += StartToListen;
            bwListener.RunWorkerAsync();
        }

        public void StopListening()
        {
            try
            {
                bwListener.CancelAsync();
                bwListener.Dispose();
                if (listenerSocket.Connected)
                    listenerSocket.Shutdown(SocketShutdown.Both);
                listenerSocket.Close();
            }
            catch (SocketException ex)
            {
                Log.Error("Socket exception occured while server tried to stop socket listeners", ex);
                throw;
            }
        }

        private void StartToListen(object sender, DoWorkEventArgs e)
        {
            listenerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            listenerSocket.Bind(new IPEndPoint(serverIP, serverPort));
            listenerSocket.Listen(GameConfig.MaxPlayersCount);
            while (true)
            {
                try
                {
                    var socket = listenerSocket.Accept();
                    socket.Blocking = false;

                    var newClientHandler = new Client(socket);
                    OnClientConnected(new ClientConnectedEventArgs(newClientHandler));
                }
                catch (SocketException ex)
                {
                    Log.Error("Socket exception occured while server waited to new clients", ex);
                    throw;
                }
                
            }
        }
    }
}