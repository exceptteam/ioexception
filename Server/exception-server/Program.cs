﻿using System;
using exception_server.Model.Gameplay;
using log4net;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace exception_server
{
    class Program 
    {
        protected static ILog Log
        {
            get { return LogManager.GetLogger(typeof (Program)); }
        }

        public static void Main(string[] args)
        {
            try
            {
                new Game().Start();
            }
            catch (Exception ex)
            {
                Log.Fatal("Some fatal exception occures.", ex);
                throw;
            }

            Console.WriteLine("Press any key to stop server");
            Console.ReadLine();
        }
    }
}
